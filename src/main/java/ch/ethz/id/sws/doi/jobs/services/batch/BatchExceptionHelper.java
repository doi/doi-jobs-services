package ch.ethz.id.sws.doi.jobs.services.batch;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.base.jobs.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doierror.DOIErrorService;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIError;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;

@Component
public class BatchExceptionHelper {

    private static Log LOG = LogFactory.getLog(BatchExceptionHelper.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private MessageSource messageSource = null;

    @Autowired
    private DOIErrorService doiErrorService = null;

    public void storeGeneralBatchError(Throwable throwable, Long executionId) {
        this.storeError(
                throwable,
                executionId,
                null,
                null,
                null,
                null,
                null);
    }

    public void storeBatchFailures(StepExecution stepExecution) {
        if (!stepExecution.getFailureExceptions().isEmpty()) {
            Throwable throwable = stepExecution.getFailureExceptions().get(0);

            this.storeError(
                    throwable,
                    stepExecution.getJobExecutionId(),
                    this.getDOIPoolId(stepExecution),
                    null,
                    this.getStacktrace(throwable),
                    null,
                    null);
        }
    }

    public void storeDOIRelatedError(Throwable throwable, Long executionId, DomObjDOI domObjDOI) {
        this.storeError(
                throwable,
                executionId,
                domObjDOI.getDoiPoolId(),
                domObjDOI.getId(),
                domObjDOI.getMetadataJson(),
                null,
                null);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void storePoolConfigError(Throwable throwable, Long executionId, DomObjDOIPool domObjDOIPool) {
        this.storeError(
                throwable,
                executionId,
                domObjDOIPool.getId(),
                null,
                null,
                null,
                null);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void storeImportRequestError(Throwable throwable, Long executionId, DomObjDOIPool domObjDOIPool,
            String oaipmhRequest, String oaipmhResponse) {
        this.storeError(
                throwable,
                executionId,
                domObjDOIPool.getId(),
                null,
                null,
                oaipmhRequest,
                oaipmhResponse);
    }

    public void storeImportError(Throwable throwable, Long executionId, DomObjDOIPool domObjDOIPool,
            String doi, String dublinCoreSnipplet) {
        this.storeError(
                throwable,
                executionId,
                domObjDOIPool.getId(),
                null,
                dublinCoreSnipplet,
                null,
                null);
    }

    public void storeImportWarning(Throwable throwable, Long executionId, Long doiPoolId,
            DomObjDOI domObjDOI, String dublinCoreSnipplet) {
        this.storeError(
                throwable,
                executionId,
                doiPoolId,
                domObjDOI.getId(),
                dublinCoreSnipplet,
                null,
                null);
    }

    public void storeExportError(Throwable throwable, Long executionId, DomObjDOIPool domObjDOIPool,
            DomObjDOI domObjDOI,
            String dataCiteRequest, String dataCiteReponse) {
        this.storeError(
                throwable,
                executionId,
                domObjDOIPool.getId(),
                domObjDOI.getId(),
                domObjDOI.getMetadataJson(),
                dataCiteRequest,
                dataCiteReponse);
    }

    public void storeComparisonError(Long executionId, DomObjDOIPool domObjDOIPool, DomObjDOI domObjDOI,
            String dataCiteDOIVersion) {
        this.storeError(
                null,
                executionId,
                domObjDOIPool.getId(),
                domObjDOI.getId(),
                domObjDOI.getMetadataJson(),
                null,
                dataCiteDOIVersion);
    }

    protected void storeError(Throwable throwable, Long executionId, Long doiPoolId, Long doiId,
            String snipplet, String request, String response) {
        try {
            DomObjDOIError domObjDOIError = new DomObjDOIError();
            domObjDOIError.setExecutionId(executionId);
            domObjDOIError.setDoiPoolId(doiPoolId);
            domObjDOIError.setDoiId(doiId);
            domObjDOIError.setHandled(0);
            domObjDOIError.setSnipplet(snipplet);
            domObjDOIError.setRequest(request);
            domObjDOIError.setResponse(response);

            if (throwable != null) {
                if (throwable instanceof ServiceException) {
                    ServiceException serviceException = (ServiceException) throwable;
                    domObjDOIError.setErrorMsg(this.getServiceMessage(serviceException));
                    domObjDOIError.setErrorCode(serviceException.getErrorCode());
                } else {
                    domObjDOIError
                            .setErrorMsg(this.getServiceMessage(DOIBatchErrors.BATCH_TECHNICAL_ERROR,
                                    new Object[] { throwable.getLocalizedMessage(), this.getStacktrace(throwable) }));
                    domObjDOIError.setErrorCode(DOIBatchErrors.BATCH_TECHNICAL_ERROR);
                }
            } else {
                domObjDOIError
                        .setErrorMsg(this.getServiceMessage(DOIBatchErrors.BATCH_COMPARE_MISMATCH,
                                new Object[] {}));
                domObjDOIError.setErrorCode(DOIBatchErrors.BATCH_COMPARE_MISMATCH);
            }

            this.doiErrorService.insertDOIError(domObjDOIError);
        } catch (Exception e) {
            LOG.fatal("Could not insert doi error object: ", e);
        }
    }

    private String getServiceMessage(final ServiceException exception) {
        try {
            Object[] args = null;
            if (exception.getErrors() != null && !exception.getErrors().isEmpty()) {
                args = exception.getErrors().get(0).getArguments();
            }

            String msg = this.getServiceMessage(exception.getErrorCode(), args);

            return msg;
        } catch (final Throwable e) {
            LOG.warn("Missing error message for service exception:", e);
        }

        return this.messageSource.getMessage(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR,
                new Object[] { "No error msg defined for " + exception.getErrorCode() }, Locale.getDefault());
    }

    private String getServiceMessage(String errorCode, Object[] args) throws Exception {
        String msg = this.messageSource.getMessage(errorCode, args, Locale.getDefault());

        if (msg != null && msg.length() > 508) {
            return msg.substring(0, 508) + "...";
        }

        return msg;
    }

    private String getStacktrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);

        return sw.toString();
    }

    private Long getDOIPoolId(StepExecution stepExecution) {
        try {
            final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                    stepExecution.getJobExecution());

            return batchParams.getAnyStepParamAsLong(
                    ImportBatchParameters.PARAM_DOIPOOLID,
                    null);
        } catch (Throwable t) {
        }

        return null;
    }
}
