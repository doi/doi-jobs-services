package ch.ethz.id.sws.doi.jobs.services;

public interface Constants {

    public final static String PROP_BATCH_RESTART = "restart-batches";

    public final static String PROP_APP_UNIQUEID = "app.unique-id";

    public final static String PROP_REPORT_SENDER_ADDRESS = "report.sender-address";

    public final static String PROP_REPORT_ADMINRECEIVER_ADDRESS = "admin.receiver-address";

    public final static String PROP_MAXREQUESTS_LIMIT = "export.max-requests-limit";

    public final static String PROP_MAXREQUESTS_INTERVAL = "export.max-requests-interval";

    public final static String PROP_EXPORT_POOLSIZE = "export.thread-pool-size";

    public final static String PROP_IMPORT_TIMEOUT = "import.request-timeout";
}
