package ch.ethz.id.sws.doi.jobs.services.dublincore;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;

public class AtomXMLParser implements DOIInputParser {

    public final static String ELEMENT_EPICS_RECORD = "record";

    private static Log LOG = LogFactory.getLog(AtomXMLParser.class);

    private XMLInputFactory xmlInputFactory = null;

    public AtomXMLParser() {
        this.xmlInputFactory = XMLInputFactory.newInstance();
        this.xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);
    }

    @Override
    public List<DublinCoreRecord> parseRecords(final InputStream inputStream) throws Exception {
        final InputStream bufferedInputStream = new BufferedInputStream(inputStream);
        final List<DublinCoreRecord> oaidcRecordList = new ArrayList<DublinCoreRecord>();
        final XMLEventReader reader = this.xmlInputFactory.createXMLEventReader(bufferedInputStream, "UTF-8");

        try {
            StringBuffer elementData = null;
            StringWriter rawXmlWriter = null;
            DublinCoreRecord currentRecord = null;

            while (reader.hasNext()) {
                final XMLEvent nextEvent = reader.nextEvent();

                if (nextEvent.isStartElement() && currentRecord == null) {
                    final StartElement startElement = nextEvent.asStartElement();

                    if (ELEMENT_EPICS_RECORD.equals(startElement.getName().getLocalPart())) {
                        rawXmlWriter = new StringWriter();
                        currentRecord = new DublinCoreRecord();
                    }
                } else if (nextEvent.isStartElement() && currentRecord != null) {
                    final StartElement startElement = nextEvent.asStartElement();
                    // TODO: What should happen with the language attribute?
                    final String lang = this.getLanguageAttribute(startElement);
                    elementData = new StringBuffer();
                } else if (nextEvent.isEndElement() && currentRecord != null) {
                    final EndElement endElement = nextEvent.asEndElement();

                    switch (endElement.getName().getLocalPart()) {
                        case ELEMENT_EPICS_RECORD:
                            nextEvent.writeAsEncodedUnicode(rawXmlWriter);

                            if (currentRecord != null) {
                                currentRecord.setRawXml(rawXmlWriter.toString());
                                oaidcRecordList.add(currentRecord);
                                currentRecord = null;
                            }
                            rawXmlWriter = null;
                            break;
                        case DublinCoreRecord.ELEMENT_CONTRIBUTOR:
                            this.addToList(currentRecord.getContributorList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_CREATOR:
                            this.addToList(currentRecord.getCreatorList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_DATE:
                            this.addToList(currentRecord.getDateList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_DESCRIPTION:
                            this.addToList(currentRecord.getDescriptionList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_FORMAT:
                            this.addToList(currentRecord.getFormatList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_IDENTIFIER:
                            this.addToList(currentRecord.getIdentifierList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_LANGUAGE:
                            this.addToList(currentRecord.getLanguageList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_PUBLISHER:
                            this.addToList(currentRecord.getPublisherList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_SUBJECT:
                            this.addToList(currentRecord.getSubjectList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_TITLE:
                            this.addToList(currentRecord.getTitleList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_TYPE:
                            this.addToList(currentRecord.getTypeList(), elementData);
                            elementData = null;
                            break;
                        case DublinCoreRecord.ELEMENT_RIGHTS:
                            this.addToList(currentRecord.getRightsList(), elementData);
                            elementData = null;
                            break;
                        default:
                            elementData = null;
                            break;
                    }

                } else {
                    if (elementData != null && nextEvent.isCharacters()) {
                        elementData.append(nextEvent.asCharacters().getData());
                    }
                }

                if (rawXmlWriter != null) {
                    nextEvent.writeAsEncodedUnicode(rawXmlWriter);
                }
            }

            return oaidcRecordList;
        } catch (Exception e) {
            bufferedInputStream.mark(0);
            bufferedInputStream.reset();
            String srcData = new String(bufferedInputStream.readAllBytes(), StandardCharsets.UTF_8);

            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { srcData },
                    DOIBatchErrors.BATCH_XMLPARSING_FAILED, e);
        }

        return null;
    }

    private void addToList(List<String> stringList, StringBuffer newListElement) {
        if (!StringUtils.isEmpty(newListElement.toString())) {
            stringList.add(newListElement.toString());
        }
    }

    private String getLanguageAttribute(final StartElement startElement) {
        final Attribute attrLanguage = startElement.getAttributeByName(new QName("lang"));
        if (attrLanguage != null) {
            return attrLanguage.getValue();
        }

        return null;
    }

    @Override
    public String getResumptionToken() {
        return null;
    }

    @Override
    public LocalDateTime getResponseDate() {
        return null;
    }
}
