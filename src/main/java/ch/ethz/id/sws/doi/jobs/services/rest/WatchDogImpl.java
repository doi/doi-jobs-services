package ch.ethz.id.sws.doi.jobs.services.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.ethz.id.sws.base.commons.watchdog.WatchDog;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolSuche;

@Component
public class WatchDogImpl implements WatchDog {

    @Autowired
    private DOIPoolDAO dOIPoolDAO = null;

    @Override
    public void check() throws Throwable {
        this.checkDatabase();
        this.checkBatchSystem();
    }

    private void checkDatabase() {
        DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        domObjDOIPoolSuche.setBatchStatusCode(DOIBatchStatus.VAL_IDLE);
        this.dOIPoolDAO.searchDOIPoolCount(domObjDOIPoolSuche);
    }

    private void checkBatchSystem() {

    }
}
