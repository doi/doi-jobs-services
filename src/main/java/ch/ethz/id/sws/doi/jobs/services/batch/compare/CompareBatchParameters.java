package ch.ethz.id.sws.doi.jobs.services.batch.compare;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;

public class CompareBatchParameters extends BatchParameters {

    public static String COMPARESTEP_NAME = "compareStep";

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public CompareBatchParameters() {

        this.defineStepParam(COMPARESTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(COMPARESTEP_NAME, PARAM_DOIPOOLID, "DOI pool id.");

        this.defineStepParam(COMPARESTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(COMPARESTEP_NAME, PARAM_CREATOR, "Creating user.");

        this.defineStepParam(COMPARESTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(COMPARESTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(COMPARESTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(COMPARESTEP_NAME, PARAM_DESCRIPTION, "Free description.");
    }
}
