package ch.ethz.id.sws.doi.jobs.services.batch.clear;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class ClearPoolBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job clearBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step clearPoolStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<ClearBatchParameters>(ClearBatchParameters.class),
                DOIPoolService.CLEAR_BATCH);

        return jobBuilderFactory.get(DOIPoolService.CLEAR_BATCH)
                .listener(batchStatusUpdater)
                .start(clearPoolStep)
                .build();
    }

    @Bean
    public Step clearPoolStep(StepBuilderFactory stepBuilders) {
        return stepBuilders.get(ClearBatchParameters.CLEARSTEP_NAME).tasklet(this.clearPoolTasklet()).build();
    }

    @Bean
    @StepScope
    public ClearTasklet clearPoolTasklet() {
        return new ClearTasklet();
    }
}
