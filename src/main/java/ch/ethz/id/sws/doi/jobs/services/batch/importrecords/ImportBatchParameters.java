package ch.ethz.id.sws.doi.jobs.services.batch.importrecords;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;

public class ImportBatchParameters extends BatchParameters {

    public static String IMPORTSTEP_NAME = "importStep";

    public final static String PARAM_FROMTIMESTAMP = "from";

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public ImportBatchParameters() {
        this.defineStepParam(IMPORTSTEP_NAME, PARAM_FROMTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(IMPORTSTEP_NAME, PARAM_FROMTIMESTAMP, "Start date/time.");

        this.defineStepParam(IMPORTSTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(IMPORTSTEP_NAME, PARAM_DOIPOOLID, "DOI pool id.");

        this.defineStepParam(IMPORTSTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(IMPORTSTEP_NAME, PARAM_CREATOR, "Creating user.");

        this.defineStepParam(IMPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(IMPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(IMPORTSTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(IMPORTSTEP_NAME, PARAM_DESCRIPTION, "Free description.");
    }
}
