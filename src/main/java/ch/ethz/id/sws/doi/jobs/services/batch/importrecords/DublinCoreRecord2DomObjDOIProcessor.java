package ch.ethz.id.sws.doi.jobs.services.batch.importrecords;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.commons.validation.BaseValidator;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchExceptionHelper;

public class DublinCoreRecord2DomObjDOIProcessor
        implements ItemProcessor<DublinCoreRecord, DomObjDOI> {

    private static Log LOG = LogFactory.getLog(DublinCoreRecord2DomObjDOIProcessor.class);

    @Autowired
    private final BaseValidator validator = null;

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private BatchExceptionHelper batchExceptionHelper = null;

    private StepExecution stepExecution = null;

    private LocalDateTime untilDateTime = null;

    protected DomObjDOIPool domObjDOIPool = null;

    @BeforeStep
    public void BeforeStep(final StepExecution stepExecution) throws Exception {
        this.stepExecution = stepExecution;

        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                stepExecution.getJobExecution());

        Long doiPoolId = batchParams.getStepParamAsLong(
                stepExecution.getStepName(),
                ImportBatchParameters.PARAM_DOIPOOLID,
                null);

        this.domObjDOIPool = this.doiPoolService.getDOIPool(doiPoolId);
    }

    @BeforeProcess
    public void beforeProcess(final DublinCoreRecord dublinCoreRecord) throws Exception {
        if (this.untilDateTime == null) {
            // The untilDateTime is set by the reader as it receives it in the first
            // request. Hence, we need to catch it right before processing.
            if (this.stepExecution.getExecutionContext().containsKey(this.stepExecution.getStepName() + ".jsonState")) {
                final ImportBatchState stepState = ImportBatchState
                        .createFromJSON(this.stepExecution.getExecutionContext()
                                .getString(this.stepExecution.getStepName() + ".jsonState"));

                this.untilDateTime = stepState.getUntilTimestampAsLocalDateTime();
            }
        }
    }

    @Override
    public DomObjDOI process(final DublinCoreRecord dublinCoreRecord) throws Exception {
        try {
            this.setDOIToLowercase(dublinCoreRecord);

            DomObjDOI domObjDOI = new DomObjDOI();
            domObjDOI.setDoiPoolId(this.domObjDOIPool.getId());
            domObjDOI.setDoi(dublinCoreRecord.getDoi(this.domObjDOIPool.getDoiPrefix()));
            domObjDOI.setUrl(dublinCoreRecord.getUrl(this.domObjDOIPool.getUrlPrefix(), this.domObjDOIPool.getDoiTombstone()));

            domObjDOI.setImportDate(this.untilDateTime);
            domObjDOI.setMetadataRecord(dublinCoreRecord);
            this.validator.validateObject(domObjDOI);

            // Clear out raw xml after validation and set again
            dublinCoreRecord.setRawXml(null);
            domObjDOI.setMetadataRecord(dublinCoreRecord);

            return domObjDOI;
        } catch (final Exception e) {
            this.batchExceptionHelper.storeImportError(
                    e,
                    this.stepExecution.getJobExecutionId(),
                    this.domObjDOIPool,
                    dublinCoreRecord.getDoi(this.domObjDOIPool.getDoiPrefix()),
                    dublinCoreRecord.getRawXml());
            LOG.warn("Could not convert OAIDC record: ", e);
        }

        return null;
    }

    private void setDOIToLowercase(DublinCoreRecord dublinCoreRecord) {
        String doi = dublinCoreRecord.getDoi(this.domObjDOIPool.getDoiPrefix());
        Integer doiIndex = dublinCoreRecord.getDoiIndex(this.domObjDOIPool.getDoiPrefix());

        if (!StringUtils.isEmpty(doi) && doiIndex != null) {
            String loweredDoi = dublinCoreRecord.getIdentifierList().get(doiIndex).replace(doi, doi.toLowerCase());
            dublinCoreRecord.getIdentifierList().set(doiIndex, loweredDoi);
        }
    }
}
