package ch.ethz.id.sws.doi.jobs.services.batch.delete;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;

public class DeleteBatchParameters extends BatchParameters {

    public static String DELETESTEP_NAME = "deleteStep";

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public DeleteBatchParameters() {

        this.defineStepParam(DELETESTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(DELETESTEP_NAME, PARAM_DOIPOOLID, "DOI pool id");

        this.defineStepParam(DELETESTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(DELETESTEP_NAME, PARAM_CREATOR, "Creating user");

        this.defineStepParam(DELETESTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(DELETESTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(DELETESTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(DELETESTEP_NAME, PARAM_DESCRIPTION, "Free description");
    }
}
