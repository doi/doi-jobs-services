package ch.ethz.id.sws.doi.jobs.services.batch.exportrecords;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class ExportBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job exportBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step exportStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<ExportBatchParameters>(ExportBatchParameters.class),
                DOIPoolService.EXPORT_BATCH);

        return jobBuilderFactory.get(DOIPoolService.EXPORT_BATCH)
                .listener(batchStatusUpdater)
                .start(exportStep)
                .build();
    }

    @Bean
    public Step exportStep(
            final StepBuilderFactory stepBuilderFactory,
            final TaskExecutor taskExecutor) {
        return stepBuilderFactory.get(ExportBatchParameters.EXPORTSTEP_NAME)
                .<DomObjDOI, DomObjDOI> chunk(200)
                .reader(this.getDomObjDOIReader())
                .processor(this.getDomObjDOI2DataCiteRecordProcessor())
                .writer(this.getDataCiteRecordsWriter())
                .build();
    }

    @Bean
    @StepScope
    public DomObjDOIReader getDomObjDOIReader() {
        return new DomObjDOIReader();
    }

    @Bean
    @StepScope
    public DomObjDOI2DataCiteRecordProcessor getDomObjDOI2DataCiteRecordProcessor() {
        return new DomObjDOI2DataCiteRecordProcessor();
    }

    @Bean
    @StepScope
    public DataCiteRecordWriter getDataCiteRecordsWriter() {
        return new DataCiteRecordWriter();
    }
}
