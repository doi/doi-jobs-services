package ch.ethz.id.sws.doi.jobs.services.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.doibatchcontrol.DOIBatchControlService;
import ch.ethz.id.sws.doi.commons.services.doibatchcontrol.DomObjDOIBatchControl;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.DublinCoreRecordsReader;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;

public class BatchStatusUpdater implements JobExecutionListener {

    private static Log LOG = LogFactory.getLog(DublinCoreRecordsReader.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIBatchControlService doiBatchControlService = null;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        try {
            DomObjDOIBatchControl domObjDOIBatchControl = this.getDOIBatchControl(jobExecution);
            domObjDOIBatchControl.setBatchStatusCode(DOIBatchStatus.RUNNING.getCode());
            this.doiBatchControlService.updateDOIBatchControl(domObjDOIBatchControl);
        } catch (Exception e) {
            LOG.warn("Exception in batch listener: ", e);
        }
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        try {
            DomObjDOIBatchControl domObjDOIBatchControl = this.getDOIBatchControl(jobExecution);
            domObjDOIBatchControl.setBatchStatusCode(DOIBatchStatus.IDLE.getCode());
            this.doiBatchControlService.updateDOIBatchControl(domObjDOIBatchControl);
        } catch (Exception e) {
            LOG.warn("Exception in batch listener: ", e);
        }
    }

    private DomObjDOIBatchControl getDOIBatchControl(JobExecution jobExecution) throws Exception {
        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                jobExecution);

        Long doiPoolId = batchParams.getAnyStepParamAsLong(
                ImportBatchParameters.PARAM_DOIPOOLID,
                null);

        if (doiPoolId != null) {
            return this.doiBatchControlService.getBatchControlByPoolId(doiPoolId);
        }

        return null;
    }
}
