package ch.ethz.id.sws.doi.jobs.services.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ch.ethz.id.sws.base.jobs.BaseJobsConfig;

@Configuration
@EnableBatchProcessing
@Import(BaseJobsConfig.class)
@ComponentScan(basePackageClasses = ComponentScanMarker.class)
public class BatchConfig {

    public static final int MAX_SHUTDOWN_WAITTIME = 30000;
    public static final int MAX_OAIPMH_REQUEST_RETRY = 3;

    private static Log LOG = LogFactory.getLog(BatchConfig.class);

}
