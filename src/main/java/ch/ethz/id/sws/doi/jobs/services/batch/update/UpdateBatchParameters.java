package ch.ethz.id.sws.doi.jobs.services.batch.update;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.ExportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;

public class UpdateBatchParameters extends BatchParameters {

    public final static String PARAM_FROMTIMESTAMP = "from";

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public UpdateBatchParameters() {

        this.defineStepParam(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_FROMTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_FROMTIMESTAMP, "Start date/time.");

        this.defineStepParam(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_FROMTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_FROMTIMESTAMP, "Start date/time.");

        this.defineStepParam(null, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(null, PARAM_DOIPOOLID, "DOI pool id");

        this.defineStepParam(null, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(null, PARAM_CREATOR, "Creating user");

        this.defineStepParam(null, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(null, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(null, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(null, PARAM_DESCRIPTION, "Free description");
    }
}
