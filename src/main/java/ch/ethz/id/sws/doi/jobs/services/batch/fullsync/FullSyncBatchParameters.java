package ch.ethz.id.sws.doi.jobs.services.batch.fullsync;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;

public class FullSyncBatchParameters extends BatchParameters {

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public FullSyncBatchParameters() {

        this.defineStepParam(null, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(null, PARAM_DOIPOOLID, "DOI pool id.");

        this.defineStepParam(null, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(null, PARAM_CREATOR, "Creating user.");

        this.defineStepParam(null, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(null, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(null, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(null, PARAM_DESCRIPTION, "Free description.");
    }
}
