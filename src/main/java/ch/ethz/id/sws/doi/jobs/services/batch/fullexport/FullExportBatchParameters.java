package ch.ethz.id.sws.doi.jobs.services.batch.fullexport;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.ExportBatchParameters;

public class FullExportBatchParameters extends BatchParameters {

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public FullExportBatchParameters() {
        this.defineStepParam(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_DOIPOOLID, "DOI pool id.");

        this.defineStepParam(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_CREATOR, "Creating user.");

        this.defineStepParam(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(ExportBatchParameters.EXPORTSTEP_NAME, PARAM_DESCRIPTION, "Free description.");
    }
}
