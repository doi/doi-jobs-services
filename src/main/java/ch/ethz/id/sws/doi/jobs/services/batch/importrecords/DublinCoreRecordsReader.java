package ch.ethz.id.sws.doi.jobs.services.batch.importrecords;

import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.Constants;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchConfig;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchExceptionHelper;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;
import ch.ethz.id.sws.doi.jobs.services.dublincore.AtomXMLParser;
import ch.ethz.id.sws.doi.jobs.services.dublincore.DOIInputParser;
import ch.ethz.id.sws.doi.jobs.services.dublincore.DublinCoreXMLParser;
import ch.ethz.id.sws.doi.jobs.services.dublincore.DublinCoreXsltTransformer;
import ch.ethz.id.sws.doi.jobs.services.dublincore.OAIPMHRequestGenerator;

public class DublinCoreRecordsReader implements ItemReader<DublinCoreRecord>, ItemStream {

    private static Log LOG = LogFactory.getLog(DublinCoreRecordsReader.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private BatchExceptionHelper batchExceptionHelper = null;

    @Autowired
    private B3ApplicationContext b3ApplicationContext = null;

    private int timeout = 0;

    private HttpClient httpClient = null;

    private OAIPMHRequestGenerator uriGenerator = null;
    private LocalDateTime fromDateTime = null;
    private LocalDateTime newLastImportDate = null;
    private DomObjDOIPool domObjDOIPool = null;

    private StepExecution stepExecution = null;
    private Boolean stopSignal = null;
    private int listCounter = 0;
    private String currentResumptionToken = null;
    private String nextResumptionToken = null;
    private List<DublinCoreRecord> oaidcRecordList = null;
    private DublinCoreXsltTransformer dublinCoreXsltTransformer = null;

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution) throws Exception {
        this.stopSignal = false;
        this.stepExecution = stepExecution;

        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                stepExecution.getJobExecution());

        this.fromDateTime = batchParams.getStepParamAsLocalDateTime(
                stepExecution.getStepName(),
                ImportBatchParameters.PARAM_FROMTIMESTAMP,
                null);

        long doiPoolId = batchParams.getStepParamAsLong(
                stepExecution.getStepName(),
                ImportBatchParameters.PARAM_DOIPOOLID,
                null);
        this.domObjDOIPool = this.doiPoolService.getDOIPool(doiPoolId);

        try {
            this.timeout = Integer
                    .parseInt(this.b3ApplicationContext.getProperty(Constants.PROP_IMPORT_TIMEOUT, "60"));
        } catch (Exception e) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_IMPORT_TIMEOUT },
                    DOIBatchErrors.BATCH_UNDEFINED_POOLSIZE);
        }

        this.newLastImportDate = LocalDateTime.now();

        this.uriGenerator = new OAIPMHRequestGenerator(this.domObjDOIPool);
        this.dublinCoreXsltTransformer = new DublinCoreXsltTransformer(this.domObjDOIPool.getXslt());

        this.httpClient = HttpClient.newBuilder()
                .connectTimeout(Duration.ofMillis(5000))
                .followRedirects(Redirect.NORMAL)
                .build();
    }

    @Override
    public DublinCoreRecord read()
            throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        if (this.oaidcRecordList.size() <= this.listCounter && this.nextResumptionToken != null && !this.stopSignal) {
            // Request next chunk
            URI requestURI = null;

            try {
                String resumptionToken = this.nextResumptionToken;

                do {
                    this.currentResumptionToken = resumptionToken;
                    requestURI = this.uriGenerator.createRequest(this.fromDateTime, resumptionToken);
                    final DOIInputParser doiInputParser = this.createParser(this.domObjDOIPool);

                    this.oaidcRecordList = doiInputParser.parseRecords(
                            this.dublinCoreXsltTransformer.transform(
                                    this.requestDataChunk(requestURI)));

                    resumptionToken = doiInputParser.getResumptionToken();
                } while (resumptionToken != null && this.oaidcRecordList.isEmpty());

                this.nextResumptionToken = resumptionToken;
                this.listCounter = 0;
            } catch (final ServiceException e) {
                String response = null;

                switch (e.getErrorCode()) {
                    case DOIBatchErrors.BATCH_XMLPARSING_FAILED:
                        response = e.getErrors().get(0).getArguments()[0].toString();
                        break;
                    case DOIBatchErrors.BATCH_XSLTTRANSFORMATION_FAILED:
                        response = e.getErrors().get(0).getArguments()[1].toString();
                        break;
                }

                this.batchExceptionHelper.storeImportRequestError(
                        e,
                        this.stepExecution.getJobExecutionId(),
                        this.domObjDOIPool,
                        requestURI.toString(),
                        response);
                throw new ItemStreamException(e);
            } catch (final Exception e) {
                this.batchExceptionHelper.storePoolConfigError(
                        e,
                        this.stepExecution.getJobExecutionId(),
                        this.domObjDOIPool);
                throw new ItemStreamException(e);
            }
        }

        if (this.oaidcRecordList.size() > this.listCounter && !this.stopSignal) {
            return this.oaidcRecordList.get(this.listCounter++);
        }

        return null;
    }

    @Override
    public void open(final ExecutionContext executionContext) throws ItemStreamException {

        URI requestURI = null;
        ImportBatchState stepState = new ImportBatchState();

        try {
            if (executionContext.containsKey(this.stepExecution.getStepName() + ".jsonState")) {
                stepState = ImportBatchState.createFromJSON(
                        executionContext.getString(this.stepExecution.getStepName() + ".jsonState"));
            } else {
                stepState.setOffset(0);
                stepState.setResumptionToken(null);
            }

            this.listCounter = stepState.getOffset();

            LocalDateTime responseDate = null;
            String resumptionToken = stepState.getResumptionToken();

            do {
                this.currentResumptionToken = resumptionToken;
                requestURI = this.uriGenerator.createRequest(this.fromDateTime, resumptionToken);
                final DOIInputParser doiInputParser = this.createParser(this.domObjDOIPool);
                responseDate = doiInputParser.getResponseDate();

                this.oaidcRecordList = doiInputParser.parseRecords(
                        this.dublinCoreXsltTransformer.transform(
                                this.requestDataChunk(requestURI)));

                resumptionToken = doiInputParser.getResumptionToken();
            } while (resumptionToken != null && this.oaidcRecordList.isEmpty());

            this.nextResumptionToken = resumptionToken;

            if (responseDate != null) {
                stepState.setUntilTimestamp(responseDate);
            } else {
                stepState.setUntilTimestamp(LocalDateTime.now());
            }

            executionContext.putString(this.stepExecution.getStepName() + ".jsonState", stepState.toJSON());
        } catch (final ServiceException e) {
            String response = null;

            switch (e.getErrorCode()) {
                case DOIBatchErrors.BATCH_XMLPARSING_FAILED:
                    response = e.getErrors().get(0).getArguments()[0].toString();
                    break;
                case DOIBatchErrors.BATCH_XSLTTRANSFORMATION_FAILED:
                    response = e.getErrors().get(0).getArguments()[1].toString();
                    break;
            }

            this.batchExceptionHelper.storeImportRequestError(
                    e,
                    this.stepExecution.getJobExecutionId(),
                    this.domObjDOIPool,
                    requestURI != null ? requestURI.toString() : null,
                    response);
            throw new ItemStreamException(e);
        } catch (final Exception e) {
            this.batchExceptionHelper.storePoolConfigError(
                    e,
                    this.stepExecution.getJobExecutionId(),
                    this.domObjDOIPool);
            throw new ItemStreamException(e);
        }
    }

    @Override
    public void update(final ExecutionContext executionContext) throws ItemStreamException {

        try {
            final ImportBatchState stepState = ImportBatchState
                    .createFromJSON(this.stepExecution.getExecutionContext()
                            .getString(this.stepExecution.getStepName() + ".jsonState"));
            stepState.setOffset(this.listCounter);
            stepState.setResumptionToken(this.currentResumptionToken);

            executionContext.putString(this.stepExecution.getStepName() + ".jsonState", stepState.toJSON());
        } catch (final Exception e) {
            LOG.error("Updating execution context of executionId = " + this.stepExecution.getJobExecutionId()
                    + " failed: ", e);
            throw new ItemStreamException(e);
        }
    }

    @Override
    public void close() throws ItemStreamException {
        if (this.stepExecution != null) {
            LOG.debug("Closing doiBatchId = " + this.stepExecution.getJobExecutionId());
        }
    }

    @AfterStep
    public void afterStep(final StepExecution stepExecution) {
        if (stepExecution.getFailureExceptions().isEmpty()) {
            DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(this.domObjDOIPool.getId());

            try {
                domObjDOIPool.setLastImportDate(this.newLastImportDate);
                this.doiPoolService.updateDOIPool(domObjDOIPool);
            } catch (final Exception e) {
                LOG.fatal("Exception during doiPool update: ", e);
                throw new ItemStreamException(
                        "Could not execute beforeStep for executionId = " + stepExecution.getJobExecutionId() + ": ",
                        e);
            }
        }

        // The signal flag is confirmed here to ensure that all step-related tasks in
        // reader, processor and writer can be completed.
        this.stopSignal = null;
    }

    @PreDestroy
    protected void preDestroy() {
        if (this.stopSignal == null) {
            // Reader not yet started
            return;
        } else if (!this.stopSignal) {
            // Reader started
            LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                    + " received stop signal - stopping job...");

            this.stopSignal = true;
        }

        long waitStart = System.currentTimeMillis();
        while (System.currentTimeMillis() - waitStart < BatchConfig.MAX_SHUTDOWN_WAITTIME) {
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                        + " non-gracefully stopped.");
                break;
            }

            if (this.stopSignal == null) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId() + " gracefully stopped.");
                break;
            }
        }
    }

    private InputStream requestDataChunk(final URI requestURI) throws Exception {
        int retryCounter = 0;
        int httpStatus = 0;

        do {
            try {
                LOG.info("Submitting OAIPMH request try #" + (++retryCounter) + " to '" + requestURI.toString() + "'");

                final HttpRequest httpRequest = HttpRequest.newBuilder()
                        .uri(requestURI)
                        .timeout(Duration.ofSeconds(this.timeout))
                        .header("Content-Type", "text/xml")
                        .GET()
                        .build();

                final HttpResponse<InputStream> httpResponse = this.httpClient.send(httpRequest,
                        BodyHandlers.ofInputStream());
                httpStatus = httpResponse.statusCode();

                if (HttpStatus.Series.resolve(httpStatus) != HttpStatus.Series.SUCCESSFUL) {
                    if (httpStatus == HttpStatus.SERVICE_UNAVAILABLE.value()) {
                        try {
                            long timeToWait = httpResponse.headers().firstValueAsLong("Retry-After").getAsLong();
                            LOG.info("We're requested to wait " + timeToWait + " seconds and will try again.");
                            Thread.sleep(timeToWait * 1000);
                        } catch (Exception e) {
                            retryCounter = BatchConfig.MAX_OAIPMH_REQUEST_RETRY;
                            LOG.warn("Sleep time for next request interrupted: ", e);
                        }
                    } else {
                        LOG.info("OAIPMH request for executionId = " + this.stepExecution.getJobExecutionId()
                                + " failed with status = "
                                + httpStatus);
                    }
                } else {
                    return httpResponse.body();
                }
            } catch (Exception e) {
                LOG.warn("OAIPMH request failed: ", e);
            }
        } while (retryCounter < BatchConfig.MAX_OAIPMH_REQUEST_RETRY);

        DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                new Object[] { requestURI.toString(), httpStatus },
                DOIBatchErrors.BATCH_IMPORTREQUEST_FAILED);

        return null;
    }

    private DOIInputParser createParser(DomObjDOIPool domObjDOIPool) throws Exception {
        switch (domObjDOIPool.getImportTypeCode()) {
            case 1:
                return new DublinCoreXMLParser();
            case 2:
                return new AtomXMLParser();
            case 0:
            default:
                DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                        new Object[] { domObjDOIPool.getName(), domObjDOIPool.getImportTypeCode() },
                        DOIBatchErrors.BATCH_INVALID_IMPORTTYPE);
        }

        return null;
    }
}
