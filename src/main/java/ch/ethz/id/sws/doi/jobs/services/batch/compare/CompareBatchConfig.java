package ch.ethz.id.sws.doi.jobs.services.batch.compare;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.DomObjDOI2DataCiteRecordProcessor;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.DomObjDOIReader;

@Configuration
public class CompareBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job compareBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step compareStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<CompareBatchParameters>(CompareBatchParameters.class),
                DOIPoolService.COMPARE_BATCH);

        return jobBuilderFactory.get(DOIPoolService.COMPARE_BATCH)
                .listener(batchStatusUpdater)
                .start(compareStep)
                .build();
    }

    @Bean
    public Step compareStep(
            final StepBuilderFactory stepBuilderFactory,
            final DomObjDOIReader domObjDOIReader,
            final DomObjDOI2DataCiteRecordProcessor domObjDOI2DataCiteRecordProcessor,
            final TaskExecutor taskExecutor) {
        return stepBuilderFactory.get(CompareBatchParameters.COMPARESTEP_NAME)
                .<DomObjDOI, DomObjDOI> chunk(100)
                .reader(domObjDOIReader)
                .processor(domObjDOI2DataCiteRecordProcessor)
                .writer(this.getDataCiteRecordsComparator())
                .build();
    }

    @Bean
    @StepScope
    public ItemWriter<DomObjDOI> getDataCiteRecordsComparator() {
        return new DataCiteRecordComparator();
    }
}
