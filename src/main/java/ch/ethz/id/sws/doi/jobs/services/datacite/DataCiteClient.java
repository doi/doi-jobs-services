package ch.ethz.id.sws.doi.jobs.services.datacite;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.commons.services.repository.ServiceRepository;
import ch.ethz.id.sws.doi.jobs.services.Constants;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DataCiteClient {

    private static Log LOG = LogFactory.getLog(DataCiteClient.class);

    private static final String ENDPOINT_ID = "datacite-doi-services-v2";

    private static int maxRequestsLimit = 3000;
    private static int maxRequestsInterval = 0;

    private static int requestsThisInterval = 0;
    private static long nowStart = System.currentTimeMillis();

    @Autowired
    private ServiceRepository serviceRepository = null;

    @Autowired
    private B3ApplicationContext b3ApplicationContext = null;

    private URI uri = null;

    private RestTemplate restTemplate = null;

    @PostConstruct
    public void init() throws Exception {
        this.setAlternateUri(ENDPOINT_ID);

        this.restTemplate = new RestTemplate();

        try {
            maxRequestsLimit = Integer
                    .parseInt(this.b3ApplicationContext.getProperty(Constants.PROP_MAXREQUESTS_LIMIT));
        } catch (Exception e) {
            throw new IllegalArgumentException("Property '" + Constants.PROP_MAXREQUESTS_LIMIT
                    + "' not found - please provide max request limit for datacite REST interface usage.", e);
        }

        try {
            maxRequestsInterval = Integer
                    .parseInt(this.b3ApplicationContext.getProperty(Constants.PROP_MAXREQUESTS_INTERVAL));
        } catch (Exception e) {
            throw new IllegalArgumentException("Property '" + Constants.PROP_MAXREQUESTS_INTERVAL
                    + "' not found - please provide max request interval for datacite REST interface usage.", e);
        }
    }

    public void setAlternateUri(String endpointId) throws Exception {
        this.uri = new URI(this.serviceRepository.getUrlForService(endpointId));

        if (StringUtils.isEmpty(this.uri)) {
            LOG.fatal("Could not resolve rest service endpoint for '" + endpointId
                    + "' - no declaration in properties file.");

            throw new IllegalArgumentException("Could not resolve rest service endpoint for '" + endpointId
                    + "' - no declaration in properties file.");
        }
    }

    public void setCredentials(final String username, final String password) {
        final HttpHost host = new HttpHost(this.uri.getHost(), this.uri.getPort(), this.uri.getScheme());
        final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactoryBasicAuth(
                host);

        this.restTemplate = new RestTemplate(requestFactory);
        this.restTemplate.getInterceptors()
                .add(new BasicAuthenticationInterceptor(username, password, Charset.forName("UTF-8")));
    }

    public String addDOI(final DataCiteDataWrapper dataCiteDataWrapper) throws Exception {
        LOG.debug("POST REST call " + ENDPOINT_ID + "/dois/ for " + dataCiteDataWrapper.getData().getId());
        long startTick = System.currentTimeMillis();

        try {
            incRequests();
            ResponseEntity<String> response = this.restTemplate.postForEntity(
                    this.uri + "/dois",
                    dataCiteDataWrapper,
                    String.class);

            return response.getBody();
        } catch (final HttpClientErrorException e) {
            LOG.info("DataCite POST REST call " + ENDPOINT_ID + "/dois/ failed:", e);
            throw e;
        } catch (final Exception e) {
            LOG.info("DataCite POST REST call " + ENDPOINT_ID + "/dois/ failed:", e);

            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] {
                            dataCiteDataWrapper.getData().getId(),
                            "" },
                    DOIBatchErrors.BATCH_DATACITE_REQUEST_FAILED, e);
            return null;
        } finally {
            LOG.debug("DataCite POST REST call " + ENDPOINT_ID + "/dois/ completed in "
                    + (System.currentTimeMillis() - startTick) + "ms");
        }
    }

    public String updateDOI(final DataCiteDataWrapper dataCiteDataWrapper) throws Exception {
        LOG.debug("PUT REST call " + ENDPOINT_ID + "/dois/" + dataCiteDataWrapper.getData().getId());
        long startTick = System.currentTimeMillis();

        try {
            incRequests();
            RequestEntity<DataCiteDataWrapper> requestEntity = RequestEntity
                    .put(new URI(this.uri + "/dois/" + dataCiteDataWrapper.getData().getId()))
                    .body(dataCiteDataWrapper);

            ResponseEntity<String> response = this.restTemplate.exchange(requestEntity, String.class);

            return response.getBody();
        } catch (final HttpClientErrorException e) {
            LOG.info("DataCite PUT REST call " + ENDPOINT_ID + "/dois/ failed:", e);
            throw e;
        } catch (final Exception e) {
            LOG.info("DataCite PUT REST call " + ENDPOINT_ID + "/dois/ failed:", e);

            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] {
                            dataCiteDataWrapper.getData().getId(),
                            "[unknown]",
                            e.toString() },
                    DOIBatchErrors.BATCH_DATACITE_REQUEST_FAILED, e);
            return null;
        } finally {
            LOG.debug("DataCite PUT REST call " + ENDPOINT_ID + "/dois/ completed in "
                    + (System.currentTimeMillis() - startTick) + "ms");
        }
    }

    public void deleteDOI(final String doi) {
        LOG.debug("DELETE REST call " + ENDPOINT_ID + "/dois/" + doi);
        long startTick = System.currentTimeMillis();

        try {
            incRequests();
            this.restTemplate.delete(this.uri + "/dois/" + doi);
        } catch (final Exception e) {
            LOG.info("DELETE REST call " + ENDPOINT_ID + "/dois/failed:", e);
            throw e;
        } finally {
            LOG.debug("DELETE REST call " + ENDPOINT_ID + "/dois/ completed in "
                    + (System.currentTimeMillis() - startTick) + "ms");
        }
    }

    public DataCiteDataWrapper getDOI(final String doi) {
        LOG.debug("GET REST call " + ENDPOINT_ID + "/dois/" + doi);
        long startTick = System.currentTimeMillis();

        try {
            incRequests();
            DataCiteDataWrapper response = this.restTemplate
                    .getForEntity(this.uri + "/dois/" + doi, DataCiteDataWrapper.class).getBody();

            return response;
        } catch (final Exception e) {
            LOG.info("GET REST call " + ENDPOINT_ID + "/dois/ failed:", e);
            throw e;
        } finally {
            LOG.debug("GET REST call " + ENDPOINT_ID + "/dois/ completed in " + (System.currentTimeMillis() - startTick)
                    + "ms");
        }
    }

    private static void incRequests()
    {
      long currentInterval = System.currentTimeMillis() / Math.max(1, maxRequestsInterval) / 1000;
      
      if (nowStart / maxRequestsInterval / 1000 == currentInterval && maxRequestsInterval > 0)
      {
        if (requestsThisInterval >= maxRequestsLimit)
        {
          // We've reached the request limit per minute!
          // Let's wait until (maxRequestsInterval - time to reach the max. requests).
          
          long now = System.currentTimeMillis();
          
          LOG.info("Elapsed time: " + (int)((now - nowStart) / 1000));
          LOG.info("Stop time: " + new Date());
          
          int waitSec =  maxRequestsInterval - (int)((now - nowStart) / 1000);
          try
          {
        	LOG.info("DataCite requests/min limit of " + requestsThisInterval + " reached. Waiting " + waitSec + " seconds.");
            Thread.sleep(waitSec * 1000);
          }
          catch (Exception e)
          {
          }
          
          LOG.info("Waking up for DataCite requests!");

          LOG.info("Resume time: " + new Date());
          
          requestsThisInterval = 0;
          nowStart = System.currentTimeMillis();
          currentInterval = now / maxRequestsInterval / 1000;
        }
      }
      else
      {
    	LOG.info("Current DataCite requests/min = " + requestsThisInterval);

        requestsThisInterval = 0;
        nowStart = System.currentTimeMillis();
        currentInterval = nowStart / maxRequestsInterval / 1000;
      }

      requestsThisInterval++;
    }
}
