package ch.ethz.id.sws.doi.jobs.services.batch.exportrecords;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.commons.validation.BaseValidator;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchExceptionHelper;

public class DomObjDOI2DataCiteRecordProcessor implements ItemProcessor<DomObjDOI, DomObjDOI> {

    private static Log LOG = LogFactory.getLog(DomObjDOI2DataCiteRecordProcessor.class);

    @Autowired
    private final BaseValidator validator = null;

    @Autowired
    private BatchExceptionHelper batchExceptionHelper = null;

    private StepExecution stepExecution = null;

    @BeforeStep
    public void BeforeStep(final StepExecution stepExecution) throws Exception {
        this.stepExecution = stepExecution;
    }

    @Override
    public DomObjDOI process(final DomObjDOI domObjDOI) throws Exception {
        try {
            this.validator.validateObject(domObjDOI);

            return domObjDOI;
        } catch (final Exception e) {
            this.batchExceptionHelper.storeDOIRelatedError(
                    e,
                    this.stepExecution.getJobExecutionId(),
                    domObjDOI);

            LOG.warn("Could not validate DOI record with doiId = " + domObjDOI.getId() + ": ",
                    e);
        }

        return null;
    }
}
