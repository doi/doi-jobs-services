package ch.ethz.id.sws.doi.jobs.services.batch.importrecords;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class ImportBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job importBatch(
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step importStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<ImportBatchParameters>(ImportBatchParameters.class),
                DOIPoolService.IMPORT_BATCH);

        return jobBuilderFactory.get(DOIPoolService.IMPORT_BATCH)
                .listener(this.getBatchStatusUpdater())
                .start(importStep)
                .build();
    }

    @Bean
    public Step importStep(final StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get(ImportBatchParameters.IMPORTSTEP_NAME)
                .<DublinCoreRecord, DomObjDOI> chunk(100)
                .reader(this.getDublinCoreRecordsReader())
                .processor(this.getDublinCoreRecord2DomObjDOIProcessor())
                .writer(this.getDomObjDOIWriter())
                .build();
    }

    @Bean
    @StepScope
    public DublinCoreRecordsReader getDublinCoreRecordsReader() {
        return new DublinCoreRecordsReader();
    }

    @Bean
    @StepScope
    public DublinCoreRecord2DomObjDOIProcessor getDublinCoreRecord2DomObjDOIProcessor() {
        return new DublinCoreRecord2DomObjDOIProcessor();
    }

    @Bean
    @StepScope
    public DomObjDOIWriter getDomObjDOIWriter() {
        return new DomObjDOIWriter();
    }

    @Bean
    public BatchStatusUpdater getBatchStatusUpdater() {
        return new BatchStatusUpdater();
    }

}
