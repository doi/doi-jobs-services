package ch.ethz.id.sws.doi.jobs.services.mailreport;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.util.MimeType;

import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolDashboard;
import ch.ethz.id.sws.doi.commons.services.doistatistics.DomObjDOIStatistics;
import ch.ethz.id.sws.mail.queue.services.rest.v1.model.in.MailEntry;

public interface MailReportService {
    public boolean submit(MailEntry mailEntry);

    public MailEntry createMail(String senderAddress, List<String> destinationList, String subject, String content,
            MimeType mimeType);

    public boolean sendUserMailReport(LocalDateTime sinceDate, List<DomObjDOIPoolDashboard> dashboardList,
            List<DomObjDOIStatistics> statsList);

    public boolean sendAdminMailReport(LocalDateTime sinceDate, List<DomObjDOIPoolDashboard> dashboardList,
            List<DomObjDOIStatistics> statsList);
}
