package ch.ethz.id.sws.doi.jobs.services.batch.clear;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;

public class ClearBatchParameters extends BatchParameters {

    public static String CLEARSTEP_NAME = "clearStep";

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public ClearBatchParameters() {

        this.defineStepParam(CLEARSTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(CLEARSTEP_NAME, PARAM_DOIPOOLID, "DOI pool id");

        this.defineStepParam(CLEARSTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(CLEARSTEP_NAME, PARAM_CREATOR, "Creating user");

        this.defineStepParam(CLEARSTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(CLEARSTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(CLEARSTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(CLEARSTEP_NAME, PARAM_DESCRIPTION, "Free description");
    }
}
