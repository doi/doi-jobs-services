package ch.ethz.id.sws.doi.jobs.services.batch.clear;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchConfig;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;

public class ClearTasklet implements Tasklet {

    private static Log LOG = LogFactory.getLog(ClearTasklet.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    private StepExecution stepExecution = null;

    private Boolean stopSignal = null;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        Long doiPoolId = null;
        this.stopSignal = false;
        this.stepExecution = chunkContext.getStepContext().getStepExecution();

        try {
            final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                    this.stepExecution.getJobExecution());

            doiPoolId = batchParams.getStepParamAsLong(
                    this.stepExecution.getStepName(),
                    ImportBatchParameters.PARAM_DOIPOOLID,
                    null);

            this.doiPoolService.clearDOIPool(doiPoolId);
        } catch (Exception e) {
            LOG.error("Could not clear doiPoolId = " + doiPoolId + ": ", e);
            throw e;
        } finally {
            this.stopSignal = null;
        }

        return RepeatStatus.FINISHED;
    }

    @PreDestroy
    protected void preDestroy() {
        if (this.stopSignal == null) {
            return;
        } else if (!this.stopSignal) {
            LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                    + " received stop signal - stopping job...");

            this.stopSignal = true;
        }
        long waitStart = System.currentTimeMillis();

        while (System.currentTimeMillis() - waitStart < BatchConfig.MAX_SHUTDOWN_WAITTIME) {
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                        + " non-gracefully stopped.");
                break;
            }

            if (this.stopSignal == null) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId() + " gracefully stopped.");
                break;
            }
        }
    }
}
