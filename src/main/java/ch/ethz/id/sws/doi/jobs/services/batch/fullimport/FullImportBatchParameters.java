package ch.ethz.id.sws.doi.jobs.services.batch.fullimport;

import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;

public class FullImportBatchParameters extends BatchParameters {

    public final static String PARAM_DOIPOOLID = "doiPoolId";

    public final static String PARAM_CREATOR = "createdBy";

    public final static String PARAM_CREATIONTIMESTAMP = "createdAt";

    public final static String PARAM_DESCRIPTION = "desc";

    public FullImportBatchParameters() {
        this.defineStepParam(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_DOIPOOLID, ParamType.LONG);
        this.setStepParamDesc(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_DOIPOOLID, "DOI pool id.");

        this.defineStepParam(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_CREATOR, ParamType.STRING);
        this.setStepParamDesc(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_CREATOR, "Creating user.");

        this.defineStepParam(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, ParamType.DATETIME);
        this.setStepParamDesc(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_CREATIONTIMESTAMP, "Creation time");

        this.defineStepParam(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_DESCRIPTION, ParamType.STRING);
        this.setStepParamDesc(ImportBatchParameters.IMPORTSTEP_NAME, PARAM_DESCRIPTION, "Free description.");
    }
}
