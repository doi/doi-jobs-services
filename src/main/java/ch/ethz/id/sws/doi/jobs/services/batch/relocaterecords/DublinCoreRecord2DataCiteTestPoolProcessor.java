package ch.ethz.id.sws.doi.jobs.services.batch.relocaterecords;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.DublinCoreRecord2DomObjDOIProcessor;

public class DublinCoreRecord2DataCiteTestPoolProcessor extends DublinCoreRecord2DomObjDOIProcessor {

    @Override
    public DomObjDOI process(final DublinCoreRecord dublinCoreRecord) throws Exception {
        String originalDoi = getDoi(dublinCoreRecord.getIdentifierList());
        String originalUrl = getUrl(dublinCoreRecord.getIdentifierList());

        if (!StringUtils.isEmpty(originalDoi)) {
            dublinCoreRecord.getIdentifierList().remove(originalDoi);
            String originalDoiPrefix = originalDoi.substring(0, originalDoi.indexOf("/"));
            String fakeDoi = originalDoi.replace(originalDoiPrefix, this.domObjDOIPool.getDoiPrefix());
            dublinCoreRecord.getIdentifierList().add(fakeDoi);
        }
        if (!StringUtils.isEmpty(originalUrl)) {
            dublinCoreRecord.getIdentifierList().remove(originalUrl);
            dublinCoreRecord.getIdentifierList().add(this.domObjDOIPool.getUrlPrefix() + originalDoi);
        }

        return super.process(dublinCoreRecord);
    }

    private static String getUrl(List<String> identifierList) {
        for (final String identifier : identifierList) {
            if (identifier.startsWith("http")) {
                return identifier;
            }
        }

        return null;
    }

    private static String getDoi(List<String> identifierList) {
        for (final String identifier : identifierList) {
            final String lowecaseString = identifier.toLowerCase(Locale.ENGLISH);

            if (lowecaseString.endsWith(" / doi")) {
                return identifier.substring(0, lowecaseString.indexOf(" / doi")).trim();
            } else if (lowecaseString.startsWith("doi:")) {
                return identifier.substring("doi:".length()).trim();
            } else if (identifier.startsWith("10.")) {
                return identifier;
            }
        }

        return null;
    }
}
