package ch.ethz.id.sws.doi.jobs.services.mailreport;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.jobs.batch.services.BatchService;
import ch.ethz.id.sws.base.jobs.services.jobinstance.DomObjJobInstanceResultat;
import ch.ethz.id.sws.base.jobs.services.jobinstance.DomObjJobInstanceSuche;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolDashboard;
import ch.ethz.id.sws.doi.commons.services.doistatistics.DomObjDOIStatistics;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserResultat;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserSuche;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.jobs.services.Constants;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;
import ch.ethz.id.sws.mail.queue.client.MailQueueClient;
import ch.ethz.id.sws.mail.queue.services.rest.v1.model.in.MailEntry;

@Service
public class MailReportServiceImpl implements MailReportService {

    private static Log LOG = LogFactory.getLog(MailReportServiceImpl.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm");

    @Autowired
    private UserService userService = null;

    @Autowired
    private BatchService batchService = null;

    private MailQueueClient mailQueueClient = null;

    private String mailSenderAddress = null;

    private String adminReceiverAddress = null;

    private String stagePrefix = null;

    @Autowired
    protected void init(MailQueueClient mailQueueClient, B3ApplicationContext b3ApplicationContext)
            throws ServiceException {
        this.mailQueueClient = mailQueueClient;

        String uniqueId = b3ApplicationContext.getProperty(Constants.PROP_APP_UNIQUEID);
        if (StringUtils.isEmpty(uniqueId)) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_APP_UNIQUEID },
                    DOIBatchErrors.BATCH_UNDEFINED_UNIQUEID);
        }
        this.mailQueueClient.setUniqueId(uniqueId);

        this.mailSenderAddress = b3ApplicationContext.getProperty(Constants.PROP_REPORT_SENDER_ADDRESS);
        if (StringUtils.isEmpty(this.mailSenderAddress)) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_REPORT_SENDER_ADDRESS },
                    DOIBatchErrors.BATCH_UNDEFINED_REPORTSENDER);
        }

        this.adminReceiverAddress = b3ApplicationContext.getProperty(Constants.PROP_REPORT_ADMINRECEIVER_ADDRESS);
        if (StringUtils.isEmpty(this.adminReceiverAddress)) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_REPORT_ADMINRECEIVER_ADDRESS },
                    DOIBatchErrors.BATCH_UNDEFINED_ADMINRECEIVER);
        }

        if ("PROD".equalsIgnoreCase(b3ApplicationContext.getDevelopmentLevel())) {
            this.stagePrefix = "";
        } else {
            this.stagePrefix = b3ApplicationContext.getDevelopmentLevel().toUpperCase() + ": ";
        }
    }

    @Override
    public boolean sendAdminMailReport(LocalDateTime sinceDate, List<DomObjDOIPoolDashboard> dashboardList,
            List<DomObjDOIStatistics> statsList) {
        try {
            StringBuilder bodyText = new StringBuilder("<pre style=\"font: monospace\">Dear Sir or Madam\n\n");
            bodyText.append(
                    "You receive this report as you are owner of the DOI application. ");
            bodyText.append("The following changes occurred since the last report of ");
            bodyText.append(formatter.format(sinceDate));
            bodyText.append(":\n\n\n");

            int poolCounter = 0;
            for (DomObjDOIPoolDashboard domObjDOIPoolDashboard : dashboardList) {
                if (this.hasBatchExecutionsSince(domObjDOIPoolDashboard.getId(), sinceDate)) {
                    List<DomObjDOIStatistics> poolStatsList = statsList.stream()
                            .filter(stats -> stats.getId().equals(domObjDOIPoolDashboard.getId()))
                            .collect(Collectors.toList());

                    bodyText.append(domObjDOIPoolDashboard.getName());

                    bodyText.append("\n- Last import              : ");
                    if (domObjDOIPoolDashboard.getLastImportDate() != null) {
                        bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastImportDate()));
                    } else {
                        bodyText.append("-");
                    }

                    bodyText.append("\n- Last export              : ");
                    if (domObjDOIPoolDashboard.getLastExportDate() != null) {
                        bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastExportDate()));
                    } else {
                        bodyText.append("-");
                    }

                    bodyText.append("\n- New DOIs                 : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastNewCount());
                    bodyText.append("\n- Updated DOIs             : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastUpdateCount());
                    bodyText.append("\n- Total DOIs               : ");
                    bodyText.append(domObjDOIPoolDashboard.getTotalDoiCount());
                    long errorCount = 0;
                    for (DomObjDOIStatistics domObjDOIStatistics : poolStatsList) {
                        errorCount = errorCount + domObjDOIStatistics.getCount();
                    }
                    bodyText.append("\n- New unhandled errors     : ");
                    bodyText.append(errorCount);
                    if (errorCount > 0) {
                        bodyText.append(" of following type(s):");
                        for (DomObjDOIStatistics domObjDOIStatistics : poolStatsList) {
                            bodyText.append("\n  ");
                            bodyText.append(StringUtils.rightPad(domObjDOIStatistics.getErrorCode(), 25));
                            bodyText.append(": ");
                            bodyText.append(domObjDOIStatistics.getCount());
                        }
                    }
                    bodyText.append("\n- Total unhandled errors   : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastErrorCount());
                    bodyText.append("\n\n");
                    poolCounter++;
                }
            }

            bodyText.append("\nKind regards\n");
            bodyText.append("Your DOI desk\n</pre>");

            if (poolCounter > 0) {
                MailEntry mailEntry = this.createMail(
                        this.mailSenderAddress,
                        Arrays.asList(new String[] { this.adminReceiverAddress }),
                        this.stagePrefix + "DOI administration report",
                        bodyText.toString(),
                        MediaType.TEXT_HTML);

                LOG.info("Sent admin report to: " + this.adminReceiverAddress);
                return this.submit(mailEntry);
            }
        } catch (Exception e) {
            LOG.warn("Could not create admin report mail: ", e);
        }

        return false;
    }

    @Override
    public boolean sendUserMailReport(LocalDateTime sinceDate, List<DomObjDOIPoolDashboard> dashboardList,
            List<DomObjDOIStatistics> statsList) {
        int poolCounter = 0;

        try {
            for (DomObjDOIPoolDashboard domObjDOIPoolDashboard : dashboardList) {
                if (this.hasBatchExecutionsSince(domObjDOIPoolDashboard.getId(), sinceDate)) {
                    // Search all users assigned to pool
                    DomObjUserSuche domObjUserSuche = new DomObjUserSuche();
                    domObjUserSuche.setDoiPoolId(domObjDOIPoolDashboard.getId());
                    DomObjUserResultat domObjUserResultat = this.userService.searchUser(domObjUserSuche);

                    if (domObjUserResultat.getTotalResultCount() == 0) {
                        // No users assigned to pool - ignore
                        continue;
                    }

                    List<DomObjDOIStatistics> poolStatsList = statsList.stream()
                            .filter(stats -> stats.getId().equals(domObjDOIPoolDashboard.getId()))
                            .collect(Collectors.toList());

                    StringBuilder bodyText = new StringBuilder("<pre style=\"font: monospace\">Dear Sir or Madam\n\n");
                    bodyText.append(
                            "As owner of the DOI repository '" + domObjDOIPoolDashboard.getName()
                                    + "' you receive this report. ");

                    if (domObjDOIPoolDashboard.getLastImportDate() != null) {
                        if (domObjDOIPoolDashboard.getLastExportDate() != null) {
                            bodyText.append("The last update of your repository has happened on ");
                            if (domObjDOIPoolDashboard.getLastImportDate()
                                    .isBefore(domObjDOIPoolDashboard.getLastExportDate())) {
                                bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastExportDate()));
                            } else {
                                bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastImportDate()));
                            }
                            bodyText.append(" and caused following changes:");
                        } else {
                            bodyText.append("The last update of your repository has happened on ");
                            bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastImportDate()));
                            bodyText.append(" and caused following changes:n");
                        }
                    } else if (domObjDOIPoolDashboard.getLastExportDate() != null) {
                        bodyText.append("The last update of your repository has happened on ");
                        bodyText.append(formatter.format(domObjDOIPoolDashboard.getLastExportDate()));
                        bodyText.append(" and caused following changes:");
                    }

                    bodyText.append("\n\n- New DOIs                 : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastNewCount());
                    bodyText.append("\n- Updated DOIs             : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastUpdateCount());
                    bodyText.append("\n- Total DOIs               : ");
                    bodyText.append(domObjDOIPoolDashboard.getTotalDoiCount());

                    long errorCount = 0;
                    for (DomObjDOIStatistics domObjDOIStatistics : poolStatsList) {
                        errorCount = errorCount + domObjDOIStatistics.getCount();
                    }
                    bodyText.append("\n- New unhandled errors     : ");
                    bodyText.append(errorCount);
                    if (errorCount > 0) {
                        bodyText.append(" of following type(s):");
                        for (DomObjDOIStatistics domObjDOIStatistics : poolStatsList) {
                            bodyText.append("\n   ");
                            bodyText.append(StringUtils.rightPad(domObjDOIStatistics.getErrorCode(), 25));
                            bodyText.append(": ");
                            bodyText.append(domObjDOIStatistics.getCount());
                        }
                    }
                    bodyText.append("\n- Total unhandled errors   : ");
                    bodyText.append(domObjDOIPoolDashboard.getLastErrorCount());
                    bodyText.append("\n\n");
                    if (domObjDOIPoolDashboard.getLastErrorCount() > 0) {
                        bodyText.append(
                                "Please visit the DOI Online application to investigate the error details.\n\n");
                    }
                    bodyText.append("Kind regards\n");
                    bodyText.append("Your DOI desk\n</pre>");

                    List<String> destAddressList = domObjUserResultat.getUserList().stream()
                            .filter(domObjUser -> !StringUtils.isEmpty(domObjUser.getEmail()))
                            .map(domObjUser -> domObjUser.getEmail())
                            .collect(Collectors.toList());

                    if (!destAddressList.isEmpty()) {
                        MailEntry mailEntry = this.createMail(
                                this.mailSenderAddress,
                                destAddressList,
                                this.stagePrefix + "DOI repository report for '" + domObjDOIPoolDashboard.getName()
                                        + "'",
                                bodyText.toString(),
                                MediaType.TEXT_HTML);

                        LOG.info("Sent a user report for pool " + domObjDOIPoolDashboard.getName() + " to: "
                                + destAddressList.toString());
                        this.submit(mailEntry);
                        poolCounter++;
                    }
                }
            }
        } catch (Exception e) {
            LOG.warn("Could not process error report mails: ", e);
        }

        return poolCounter > 0;
    }

    @Override
    public MailEntry createMail(String senderAddress, List<String> destinationList, String subject, String content,
            MimeType mimeType) {

        String charsetPostfix = "";
        if (mimeType.getCharset() == null) {
            charsetPostfix = "; charset=utf-8";
        }

        final MailEntry mailEntry = new MailEntry();
        mailEntry.setHeader(new MailEntry.Header());
        for (String address : destinationList) {
            mailEntry.getHeader().getRecipientsTO().add(address);
        }
        mailEntry.getHeader().setSubject(subject);
        mailEntry.getHeader().setSender(senderAddress);
        mailEntry.setBody(new MailEntry.Body());
        mailEntry.getBody().setContent(content);
        mailEntry.getBody().setContentType(mimeType.toString() + charsetPostfix);

        return mailEntry;
    }

    @Override
    public boolean submit(MailEntry mailEntry) {
        try {
            mailEntry.setSubmitMailNow(true);
            this.mailQueueClient.createMailEntry(mailEntry);

            return true;
        } catch (final Exception e) {
            LOG.error("An exception has occurred during mail submission: ", e);
        }

        return false;
    }

    private boolean hasBatchExecutionsSince(long doiPoolId, LocalDateTime sinceDate) {
        DomObjJobInstanceSuche domObjJobInstanceSuche = new DomObjJobInstanceSuche();
        domObjJobInstanceSuche.setName("doiPoolId");
        domObjJobInstanceSuche.setLongValue(doiPoolId);
        domObjJobInstanceSuche.setCreateTimeStart(sinceDate);

        DomObjJobInstanceResultat domObjJobInstanceResultat = this.batchService.searchBatch(domObjJobInstanceSuche);

        return domObjJobInstanceResultat.getTotalResultCount() > 0;
    }
}
