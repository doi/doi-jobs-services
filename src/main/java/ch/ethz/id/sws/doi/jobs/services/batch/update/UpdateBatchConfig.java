package ch.ethz.id.sws.doi.jobs.services.batch.update;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class UpdateBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job updateBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step importStep,
            final Step exportStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<UpdateBatchParameters>(UpdateBatchParameters.class),
                DOIPoolService.UPDATE_BATCH);

        return jobBuilderFactory.get(DOIPoolService.UPDATE_BATCH)
                .listener(batchStatusUpdater)
                .start(importStep)
                .next(exportStep)
                .build();
    }
}
