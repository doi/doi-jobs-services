package ch.ethz.id.sws.doi.jobs.services.schedules;

import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob;
import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob.JobType;
import ch.ethz.id.sws.base.jobs.schedule.services.JobListener;
import ch.ethz.id.sws.base.jobs.schedule.services.JobService;
import ch.ethz.id.sws.doi.commons.services.doibatchcontrol.DOIBatchControlService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;

/** This class implements listeners to job executions and updates the next schedule field of the respective pool. */
@Component
public class HarvesterScheduleListener implements JobListener {

    private static Log LOG = LogFactory.getLog(HarvesterScheduleListener.class);

    @Autowired
    private final JobService jobService = null;

    @Autowired
    private final DOIBatchControlService doiBatchControlService = null;

    public void init() {
        this.jobService.registerListener(this);
    }

    @PreDestroy
    protected void deinit() {
        this.jobService.unregisterListener(this);
    }

    @Override
    public void jobExecutionCompleted(DomObjJob domObjJob, JobExecutionException exception, boolean wasIdleRun) {
        try {
            if (domObjJob.getJobType() == JobType.TRANSIENT) {
                final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJob.getStaticParams()
                        .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                this.updateNextRun(domObjDOIPool.getId(), domObjJob);

                LOG.info("Harvest job execution completed for doiPoolId = " + domObjDOIPool.getId() + ", name = '"
                        + domObjDOIPool.getName() + "', updated next schedule.");
            }
        } catch (final Exception e) {
            LOG.error("Could not set cron schedule for unpaused harvest job '" + domObjJob.getDescription() + "': ", e);
        }
    }

    @Override
    public void jobScheduled(DomObjJob domObjJob) {
        try {
            if (domObjJob.getJobType() == JobType.TRANSIENT) {
                final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJob.getStaticParams()
                        .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                this.updateNextRun(domObjDOIPool.getId(), domObjJob);

                LOG.info("Harvest job unpaused for doiPoolId = " + domObjDOIPool.getId() + ", name = '"
                        + domObjDOIPool.getName() + "'.");
            }
        } catch (final Exception e) {
            LOG.error("Could not set cron schedule for unpaused harvest job '" + domObjJob.getDescription() + "': ", e);
        }
    }

    @Override
    public void jobUnscheduled(DomObjJob domObjJob) {
        try {
            if (domObjJob.getJobType() == JobType.TRANSIENT) {
                final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJob.getStaticParams()
                        .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                this.updateNextRun(domObjDOIPool.getId(), domObjJob);

                LOG.info("Harvest job paused for doiPoolId = " + domObjDOIPool.getId() + ", name = '"
                        + domObjDOIPool.getName() + "'.");
            }
        } catch (final Exception e) {
            LOG.error("Could not clear cron schedule for paused harvest job '" + domObjJob.getDescription() + "': ", e);
        }
    }

    @Override
    public void jobExecutionStarted(DomObjJob arg0) {
    }

    private void updateNextRun(long doiPoolId, DomObjJob domObjJob) throws ServiceException {
        // Set next run
        if (domObjJob != null) {
            if (domObjJob.getNextRun() != null) {
                LocalDateTime nextRun = domObjJob.getNextRun()
                        .toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime();

                this.doiBatchControlService.updateNextRun(doiPoolId, nextRun);
            } else {
                this.doiBatchControlService.updateNextRun(doiPoolId, null);
            }
        }
    }

}
