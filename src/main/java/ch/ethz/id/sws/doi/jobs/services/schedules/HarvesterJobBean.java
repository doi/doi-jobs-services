package ch.ethz.id.sws.doi.jobs.services.schedules;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.base.jobs.batch.services.BatchService;
import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob;
import ch.ethz.id.sws.base.jobs.schedule.services.JobResult;
import ch.ethz.id.sws.base.jobs.schedule.services.JobService;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.ExportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.update.UpdateBatchParameters;

/** The HarvesterJobBeans are programmatically generated job beans, one for each DOI pool that has a cron schedule
 * defined. The HarvesterJobBean is then regularly called according to the cron schedule to initiate an update batch job
 * for the respective DOI pool. To do so, it creates a new instance of DomObjDOIBatch. */
@Component
@DisallowConcurrentExecution
public class HarvesterJobBean extends QuartzJobBean {

    private static Log LOG = LogFactory.getLog(HarvesterJobBean.class);

    @Autowired
    private final BatchService batchService = null;

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private final JobLauncher jobLauncher = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @Override
    protected void executeInternal(final JobExecutionContext context) throws JobExecutionException {
        try {
            final DomObjJob domObjJob = (DomObjJob) context.getMergedJobDataMap().get(JobService.JOBDETAIL_DOMOBJ);
            final DomObjDOIPool domObjDOIPoolStatic = (DomObjDOIPool) domObjJob.getStaticParams()
                    .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

            // Check if there are already pending batches. If so, do nothing.
            if (!this.hasRunningOrPendingBatch(domObjDOIPoolStatic.getId())) {
                final DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(domObjDOIPoolStatic.getId());

                final BatchParametersFactory batchParamFactory = ((BatchJobRegistry) this.jobRegistry)
                        .getBatchParameters(DOIPoolService.UPDATE_BATCH);
                final UpdateBatchParameters batchParams = (UpdateBatchParameters) batchParamFactory
                        .createEmpty();
                batchParams.setStepParam(
                        ImportBatchParameters.IMPORTSTEP_NAME,
                        UpdateBatchParameters.PARAM_FROMTIMESTAMP,
                        domObjDOIPool.getLastImportDate());
                batchParams.setStepParam(
                        ExportBatchParameters.EXPORTSTEP_NAME,
                        UpdateBatchParameters.PARAM_FROMTIMESTAMP,
                        domObjDOIPool.getLastExportDate());
                batchParams.setStepParam(
                        null,
                        UpdateBatchParameters.PARAM_DESCRIPTION,
                        "Scheduled update batch for doiPoolId = " + domObjDOIPool.getId());
                batchParams.setStepParam(
                        null,
                        UpdateBatchParameters.PARAM_DOIPOOLID,
                        domObjDOIPool.getId());
                batchParams.setStepParam(
                        null,
                        UpdateBatchParameters.PARAM_CREATOR,
                        ProcessingContext.getUserOfThisThread());
                batchParams.setStepParam(
                        null,
                        UpdateBatchParameters.PARAM_CREATIONTIMESTAMP,
                        LocalDateTime.now());

                final Job job = this.jobRegistry.getJob(DOIPoolService.UPDATE_BATCH);
                final JobExecution jobExecution = this.jobLauncher.run(job, batchParams.toJobParameters());

                LOG.info("HarvesterJobBean has started new batch job for doiPoolId = " + domObjDOIPool.getId()
                        + ", name = '" + domObjDOIPool.getName() + "' with executionId = " + jobExecution.getId());

                context.setResult(new JobResult(false));
            } else {
                context.setResult(new JobResult(true));
            }
        } catch (final Exception e) {
            LOG.error("An exception has occurred in HarvesterJobBean: ", e);
            throw new JobExecutionException(e);
        }
    }

    private boolean hasRunningOrPendingBatch(long doiPoolId) {
        List<JobExecution> batchList = this.batchService.getBatchRuntimeStatus();

        for (JobExecution jobExecution : batchList) {
            try {
                BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                        jobExecution);

                Long otherDoiPoolId = batchParams.getStepParamAsLong(
                        null,
                        ImportBatchParameters.PARAM_DOIPOOLID,
                        null);

                if (Objects.equals(otherDoiPoolId, doiPoolId)) {
                    if (jobExecution.getStatus() != BatchStatus.ABANDONED &&
                            jobExecution.getStatus() != BatchStatus.COMPLETED &&
                            jobExecution.getStatus() != BatchStatus.FAILED &
                                    jobExecution.getStatus() != BatchStatus.UNKNOWN) {
                        return true;
                    }
                }
            } catch (Exception e) {
                LOG.info("Could not get batch params for executionId = " + jobExecution.getId(), e);
            }
        }

        return false;
    }
}
