package ch.ethz.id.sws.doi.jobs.services.schedules;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob;
import ch.ethz.id.sws.base.jobs.schedule.services.JobResult;
import ch.ethz.id.sws.base.jobs.schedule.services.JobService;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolDashboard;
import ch.ethz.id.sws.doi.commons.services.doistatistics.DOIStatisticsService;
import ch.ethz.id.sws.doi.commons.services.doistatistics.DomObjDOIStatistics;
import ch.ethz.id.sws.doi.jobs.services.mailreport.MailReportService;

@Component
@DisallowConcurrentExecution
public class ErrorMailJobBean extends QuartzJobBean {

    private static Log LOG = LogFactory.getLog(ErrorMailJobBean.class);

    @Autowired
    private MailReportService mailReportService = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private DOIStatisticsService doiStatisticsService = null;

    @Autowired
    private JobService jobService = null;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        LocalDateTime sinceDate = LocalDateTime.now().minusDays(1);

        DomObjJob domObjJob = this.jobService.getJobByName(context.getJobDetail().getKey().getName());
        if (domObjJob != null && domObjJob.getPreviousRun() != null) {
            sinceDate = LocalDateTime.ofInstant(domObjJob.getPreviousRun().toInstant(),
                    ZoneId.systemDefault());
        }

        List<DomObjDOIPoolDashboard> dashboardList = this.doiPoolService.getDashboard(null);
        List<DomObjDOIStatistics> poolStatsList = this.doiStatisticsService.getStatisticsForAllPools(sinceDate);

        boolean busyUser = this.mailReportService.sendUserMailReport(sinceDate, dashboardList, poolStatsList);
        boolean busyAdmin = this.mailReportService.sendAdminMailReport(sinceDate, dashboardList, poolStatsList);

        LOG.info("Error mails have been processed changes since " + sinceDate + " - admin busy flag: " + busyAdmin
                + ", user busy flag: " + busyUser);

        context.setResult(new JobResult(!busyUser && !busyAdmin));

    }
}
