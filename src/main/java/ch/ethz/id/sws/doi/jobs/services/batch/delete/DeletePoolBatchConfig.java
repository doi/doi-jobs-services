package ch.ethz.id.sws.doi.jobs.services.batch.delete;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class DeletePoolBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job deleteBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step deletePoolStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<DeleteBatchParameters>(DeleteBatchParameters.class),
                DOIPoolService.DELETE_BATCH);

        return jobBuilderFactory.get(DOIPoolService.DELETE_BATCH)
                .listener(batchStatusUpdater)
                .start(deletePoolStep)
                .build();
    }

    @Bean
    public Step deletePoolStep(StepBuilderFactory stepBuilders) {
        return stepBuilders.get(DeleteBatchParameters.DELETESTEP_NAME).tasklet(this.deletePoolTasklet()).build();
    }

    @Bean
    @StepScope
    public DeleteTasklet deletePoolTasklet() {
        return new DeleteTasklet();
    }
}
