package ch.ethz.id.sws.doi.jobs.services.batch.fullexport;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;

@Configuration
public class FullExportBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job fullExportBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step exportStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<FullExportBatchParameters>(FullExportBatchParameters.class),
                DOIPoolService.FULLEXPORT_BATCH);

        return jobBuilderFactory.get(DOIPoolService.FULLEXPORT_BATCH)
                .listener(batchStatusUpdater)
                .start(exportStep)
                .build();
    }
}
