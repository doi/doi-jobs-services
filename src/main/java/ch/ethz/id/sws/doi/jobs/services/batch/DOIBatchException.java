package ch.ethz.id.sws.doi.jobs.services.batch;

import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.validation.ObjectError;

import ch.ethz.id.sws.base.commons.ServiceException;

public class DOIBatchException extends ServiceException {
    private static final long serialVersionUID = 1246924639857339448L;

    public DOIBatchException(final Log logger, final String message, final String errorCode,
            final List<ObjectError> errors,
            final Throwable throwable) {
        super(logger, message, errorCode, errors, throwable);
    }

    public DOIBatchException(final Log logger, final String message, final String errorCode) {
        this(logger, message, errorCode, null, null);
    }

    public DOIBatchException(final Log logger, final String message, final String errorCode,
            final List<ObjectError> errors) {
        this(logger, message, errorCode, errors, null);
    }

    public DOIBatchException(final Log logger, final String errorCode) {
        this(logger, null, errorCode, null, null);
    }

    public DOIBatchException(final Log logger, final String errorCode, final List<ObjectError> errors) {
        this(logger, null, errorCode, errors, null);
    }

    public DOIBatchException(final Log logger, final String errorCode, final Throwable throwable) {
        this(logger, null, errorCode, null, throwable);
    }

    public DOIBatchException(final Log logger, final String errorCode, final List<ObjectError> errors,
            final Throwable throwable) {
        this(logger, null, errorCode, errors, throwable);
    }
}
