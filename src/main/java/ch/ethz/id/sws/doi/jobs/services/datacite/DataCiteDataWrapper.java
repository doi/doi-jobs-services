package ch.ethz.id.sws.doi.jobs.services.datacite;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord;

@JsonInclude(Include.NON_NULL)
public class DataCiteDataWrapper {
    private DataObject data = new DataObject();

    public class DataObject {
        private String id = null;
        private String type = null;
        private DataCiteRecord attributes = null;

        public String getId() {
            return this.id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public DataCiteRecord getAttributes() {
            return this.attributes;
        }

        public void setAttributes(DataCiteRecord attributes) {
            this.attributes = attributes;
        }
    }

    public DataObject getData() {
        return this.data;
    }

    public void setData(DataObject data) {
        this.data = data;
    }
}
