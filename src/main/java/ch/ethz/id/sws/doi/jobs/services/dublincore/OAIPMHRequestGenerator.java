package ch.ethz.id.sws.doi.jobs.services.dublincore;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.util.UriComponentsBuilder;

import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;

public class OAIPMHRequestGenerator {

    private static Log LOG = LogFactory.getLog(OAIPMHRequestGenerator.class);

    public final static String ELEMENT_RESUMPTIONTOKEN = "resumptionToken";
    public final static String ELEMENT_RESPONSEDATE = "responseDate";

    private DomObjDOIPool domObjDOIPool = null;

    private DateTimeFormatter dateFormatter = null;

    public OAIPMHRequestGenerator(final DomObjDOIPool domObjDOIPool) {
        this.domObjDOIPool = domObjDOIPool;
        this.dateFormatter = DateTimeFormatter.ofPattern(domObjDOIPool.getFromDatePattern());
    }

    protected URI createInitialRequest(final LocalDateTime fromDateTime) throws Exception {
        URI tmpUri = new URI(this.domObjDOIPool.getServerUrl().trim());
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromUriString(tmpUri.toString());

        if (StringUtils.isEmpty(tmpUri.getPath())) {
            uriComponentsBuilder.path("/");
        }

        uriComponentsBuilder.queryParam("verb", "ListRecords");
        if (StringUtils.isNotEmpty(this.domObjDOIPool.getSetName())) {
            uriComponentsBuilder.queryParam("set", this.domObjDOIPool.getSetName());
        }
        if (StringUtils.isNotEmpty(this.domObjDOIPool.getMetadataPrefix())) {
            uriComponentsBuilder.queryParam("metadataPrefix", this.domObjDOIPool.getMetadataPrefix());
        }
        if (fromDateTime != null) {
            uriComponentsBuilder.queryParam("from", this.dateFormatter.format(convertToUTC(fromDateTime)));
        }

        return new URI(uriComponentsBuilder.build().toUriString());
    }

    protected URI createSubsequentRequest(final String resumptionToken) throws Exception {
        URI tmpUri = new URI(this.domObjDOIPool.getServerUrl().trim());
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromUriString(tmpUri.toString());

        if (StringUtils.isEmpty(tmpUri.getPath())) {
            uriComponentsBuilder.path("/");
        }

        uriComponentsBuilder.queryParam("verb", "ListRecords");
        uriComponentsBuilder.queryParam("resumptionToken", resumptionToken);

        return new URI(uriComponentsBuilder.build().toUriString());
    }

    public URI createRequest(final LocalDateTime fromDateTime, final String resumptionToken) throws Exception {
        if (StringUtils.isEmpty(this.domObjDOIPool.getServerUrl())) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { this.domObjDOIPool.getName() },
                    DOIBatchErrors.BATCH_UNDEFINED_SERVERURL);
        }
        if (StringUtils.isEmpty(resumptionToken)) {
            return this.createInitialRequest(fromDateTime);
        }

        return this.createSubsequentRequest(resumptionToken);
    }

    private static LocalDateTime convertToUTC(final LocalDateTime localFromDateTime) {
        ZoneId fromZoneId = ZoneId.systemDefault();
        ZonedDateTime localDateTime = ZonedDateTime.of(localFromDateTime, fromZoneId);
        ZoneId toZoneId = ZoneId.of("UTC");
        ZonedDateTime utcDateTime = localDateTime.withZoneSameInstant(toZoneId);
        return utcDateTime.toLocalDateTime();
    }
}
