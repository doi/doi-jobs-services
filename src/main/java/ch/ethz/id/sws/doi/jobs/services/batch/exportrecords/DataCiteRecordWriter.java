package ch.ethz.id.sws.doi.jobs.services.batch.exportrecords;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteMapper;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord;
import ch.ethz.id.sws.doi.commons.services.doi.DOIService;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.Constants;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchExceptionHelper;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.datacite.DataCiteClient;
import ch.ethz.id.sws.doi.jobs.services.datacite.DataCiteDataWrapper;

public class DataCiteRecordWriter implements ItemWriter<DomObjDOI> {

    private static Log LOG = LogFactory.getLog(DataCiteRecordWriter.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIService doiService = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private BatchExceptionHelper batchExceptionHelper = null;

    @Autowired
    private DataCiteClient dataCiteClient = null;

    @Autowired
    private DataCiteMapper dataCiteMapper = null;

    @Autowired
    private B3ApplicationContext b3ApplicationContext = null;

    private int poolSize = 0;

    private Boolean stopSignal = false;

    private StepExecution stepExecution = null;

    private DomObjDOIPool domObjDOIPool = null;

    private Exception fatalException = null;

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution) throws Exception {
        this.fatalException = null;
        this.stepExecution = stepExecution;

        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                stepExecution.getJobExecution());

        Long doiPoolId = batchParams.getStepParamAsLong(
                stepExecution.getStepName(),
                ImportBatchParameters.PARAM_DOIPOOLID,
                null);

        this.domObjDOIPool = this.doiPoolService.getDOIPool(doiPoolId);

        try {
            this.poolSize = Integer
                    .parseInt(this.b3ApplicationContext.getProperty(Constants.PROP_EXPORT_POOLSIZE, "20"));
        } catch (Exception e) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_EXPORT_POOLSIZE },
                    DOIBatchErrors.BATCH_UNDEFINED_POOLSIZE);
        }

        this.dataCiteClient.setCredentials(this.domObjDOIPool.getDataCiteUsername(),
                new String(this.domObjDOIPool.getDataCitePasswordAsPlainText(), StandardCharsets.UTF_8));
    }

    @Override
    public void write(final List<? extends DomObjDOI> domObjDOIList) throws Exception {
        ThreadPoolTaskExecutor taskExecutor = this.getTaskExecutor();

        try {
            for (DomObjDOI domObjDOI : domObjDOIList) {
                taskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (DataCiteRecordWriter.this.fatalException == null
                                    && !DataCiteRecordWriter.this.stopSignal) {
                                ProcessingContext.pushContext(
                                        DataCiteRecordWriter.this.stepExecution.getJobExecution().getJobInstance()
                                                .getJobName() + "@"
                                                + DataCiteRecordWriter.this.stepExecution.getJobExecution().getId());
                                DataCiteRecordWriter.this.submitDOI(domObjDOI);
                            }
                        } catch (Exception e) {
                            DataCiteRecordWriter.this.fatalException = e;
                            LOG.warn("Could not process DataCiteRecord: ", e);
                        }
                    }
                });
            }
        } finally {
            taskExecutor.shutdown();
        }

        // Kick out in case of a fatal exception
        if (this.fatalException != null) {
            throw this.fatalException;
        }
    }

    private void submitDOI(DomObjDOI domObjDOI) throws Exception {
        DublinCoreRecord dublinCoreRecord = domObjDOI.getMetadataRecord();
        DataCiteDataWrapper dataCiteDataWrapper = new DataCiteDataWrapper();
        DataCiteRecord dataCiteRecord = this.dataCiteMapper.map(dublinCoreRecord, this.domObjDOIPool);
        dataCiteRecord.setEvent("publish");

        dataCiteDataWrapper.getData().setAttributes(dataCiteRecord);
        dataCiteDataWrapper.getData().setType("dois");
        dataCiteDataWrapper.getData().setId(domObjDOI.getDoi());

        try {
            try {
                this.dataCiteClient.updateDOI(dataCiteDataWrapper);
                domObjDOI.setExportDate(LocalDateTime.now());
                this.doiService.updateDOI(domObjDOI);
            } catch (HttpClientErrorException e) {
                switch (e.getStatusCode()) {
                    case NOT_FOUND:
                    case UNAUTHORIZED:
                        // Batch Fatal: Bad credentials
                        DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                                new Object[] {
                                        dataCiteDataWrapper.getData().getId(),
                                        e.getResponseBodyAsString(),
                                        e.getStatusCode() + ":" + e.getStatusText() },
                                DOIBatchErrors.BATCH_DATACITE_INVALID_CREDENTIALS, e);
                        break;
                    case UNPROCESSABLE_ENTITY:
                        // DOI Fatal: Invalid data
                        DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                                new Object[] {
                                        dataCiteDataWrapper.getData().getId(),
                                        e.getResponseBodyAsString(),
                                        e.getStatusCode() + ":" + e.getStatusText() },
                                DOIBatchErrors.BATCH_DATACITE_REQUEST_FAILED, e);
                        break;
                    case BAD_GATEWAY:
                        // Batch Fatal: check statuspage
                        DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                                new Object[] {
                                        dataCiteDataWrapper.getData().getId(),
                                        e.getResponseBodyAsString(),
                                        e.getStatusCode() + ":" + e.getStatusText() },
                                DOIBatchErrors.BATCH_DATACITE_TEMP_SERVER_ERROR, e);
                        break;
                    case INTERNAL_SERVER_ERROR:
                    case SERVICE_UNAVAILABLE:
                    case GATEWAY_TIMEOUT:
                    default:
                        // Batch Fatal: check statuspage
                        DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                                new Object[] {
                                        dataCiteDataWrapper.getData().getId(),
                                        e.getResponseBodyAsString(),
                                        e.getStatusCode() + ":" + e.getStatusText() },
                                DOIBatchErrors.BATCH_DATACITE_FATAL_SERVER_ERROR, e);
                        break;
                }
            }
        } catch (ServiceException e) {
            LOG.warn("Could not create/update DOI at DataCite: ", e);

            // Decide if fatal or not
            switch (e.getErrorCode()) {
                case DOIBatchErrors.BATCH_DATACITE_TEMP_SERVER_ERROR:
                case DOIBatchErrors.BATCH_DATACITE_FATAL_SERVER_ERROR:
                case DOIBatchErrors.BATCH_DATACITE_INVALID_CREDENTIALS:
                    throw e;
                case DOIBatchErrors.BATCH_DATACITE_REQUEST_FAILED:
                    this.batchExceptionHelper.storeExportError(
                            e,
                            this.stepExecution.getJobExecutionId(),
                            this.domObjDOIPool,
                            domObjDOI,
                            this.objectMapper.writeValueAsString(dataCiteDataWrapper),
                            e.getErrors().get(0).getArguments()[1].toString());
                    break;
            }
        } catch (Exception e) {
            LOG.warn("Exception during create/update DOI at DataCite: ", e);
            this.batchExceptionHelper.storeExportError(
                    e,
                    this.stepExecution.getJobExecutionId(),
                    this.domObjDOIPool,
                    domObjDOI,
                    this.objectMapper.writeValueAsString(dataCiteDataWrapper),
                    null);
        }
    }

    private ThreadPoolTaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(this.poolSize);
        taskExecutor.setMaxPoolSize(Integer.MAX_VALUE);
        taskExecutor.setQueueCapacity(Integer.MAX_VALUE);
        taskExecutor.setThreadNamePrefix(this.stepExecution.getStepName() + "Thread-");
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setAwaitTerminationSeconds(300);
        taskExecutor.afterPropertiesSet();

        return taskExecutor;
    }

    @AfterStep
    public ExitStatus afterStep(final StepExecution stepExecution) throws Exception {
        if (this.stopSignal) {
            stepExecution.setStatus(org.springframework.batch.core.BatchStatus.STOPPED);
            return ExitStatus.STOPPED;
        } else if (stepExecution.getFailureExceptions().isEmpty()) {
            stepExecution.setStatus(org.springframework.batch.core.BatchStatus.COMPLETED);
            return ExitStatus.COMPLETED;
        }

        this.batchExceptionHelper.storeBatchFailures(stepExecution);

        stepExecution.setStatus(org.springframework.batch.core.BatchStatus.FAILED);
        return ExitStatus.FAILED;
    }

    @PreDestroy
    protected void preDestroy() {
        this.stopSignal = true;
    }
}
