package ch.ethz.id.sws.doi.jobs.services.batch.relocaterecords;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchStatusUpdater;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.DomObjDOIWriter;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.DublinCoreRecordsReader;

@Configuration
public class RelocateBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job relocateBatch(
            final BatchStatusUpdater batchStatusUpdater,
            final BatchJobRegistry jobRegistry,
            final JobBuilderFactory jobBuilderFactory,
            final Step relocateStep) {
        jobRegistry.registerBatchParameters(
                new BatchParametersFactory<RelocateBatchParameters>(RelocateBatchParameters.class),
                DOIPoolService.RELOCATE_BATCH);

        return jobBuilderFactory.get(DOIPoolService.RELOCATE_BATCH)
                .listener(batchStatusUpdater)
                .start(relocateStep)
                .build();
    }

    @Bean
    public Step relocateStep(
            final StepBuilderFactory stepBuilderFactory,
            final DublinCoreRecordsReader dublinCoreRecordsReader,
            final DomObjDOIWriter domObjDOIWriter) {
        return stepBuilderFactory.get(RelocateBatchParameters.RELOCATESTEP_NAME)
                .<DublinCoreRecord, DomObjDOI> chunk(100)
                .reader(dublinCoreRecordsReader)
                .processor(this.getDublinCoreRecord2DataCiteTestPoolProcessor())
                .writer(domObjDOIWriter)
                .build();
    }

    @Bean
    @StepScope
    public DublinCoreRecord2DataCiteTestPoolProcessor getDublinCoreRecord2DataCiteTestPoolProcessor() {
        return new DublinCoreRecord2DataCiteTestPoolProcessor();
    }

}
