package ch.ethz.id.sws.doi.jobs.services.dublincore;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;

public class DublinCoreXsltTransformer {

    private static Log LOG = LogFactory.getLog(DublinCoreXsltTransformer.class);

    private String xslt = null;

    public DublinCoreXsltTransformer(String xslt) {
        this.xslt = xslt;
    }

    public InputStream transform(final InputStream inputStream) throws Exception {
        if (StringUtils.isNotEmpty(this.xslt)) {
            final InputStream bufferedInputStream = new BufferedInputStream(inputStream);

            try {
                final Source xmlSource = new StreamSource(bufferedInputStream);
                final Source xsltSource = new StreamSource(new ByteArrayInputStream(this.xslt.getBytes("UTF-8")));
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final Result result = new StreamResult(byteArrayOutputStream);

                final TransformerFactory transformerFactory = TransformerFactory.newInstance();
                final Transformer transformer = transformerFactory.newTransformer(xsltSource);
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                transformer.transform(xmlSource, result);

                return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            } catch (Exception e) {
                bufferedInputStream.mark(0);
                bufferedInputStream.reset();
                String srcData = new String(bufferedInputStream.readAllBytes(), StandardCharsets.UTF_8);

                DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                        new Object[] { this.xslt.getBytes("UTF-8"), srcData },
                        DOIBatchErrors.BATCH_XSLTTRANSFORMATION_FAILED);
            }
        }

        return inputStream;
    }
}
