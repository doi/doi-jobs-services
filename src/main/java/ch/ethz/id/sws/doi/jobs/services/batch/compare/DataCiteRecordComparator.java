package ch.ethz.id.sws.doi.jobs.services.batch.compare;

import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteMapper;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchExceptionHelper;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.datacite.DataCiteClient;
import ch.ethz.id.sws.doi.jobs.services.datacite.DataCiteDataWrapper;

public class DataCiteRecordComparator implements ItemWriter<DomObjDOI> {

    private static Log LOG = LogFactory.getLog(DataCiteRecordComparator.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private BatchExceptionHelper batchExceptionHelper = null;

    @Autowired
    private DataCiteMapper dataCiteMapper = null;

    @Autowired
    private DataCiteClient dataCiteClient = null;

    private Boolean stopSignal = false;

    private StepExecution stepExecution = null;

    private DomObjDOIPool domObjDOIPool = null;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution) throws Exception {
        this.stepExecution = stepExecution;

        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                stepExecution.getJobExecution());

        Long doiPoolId = batchParams.getStepParamAsLong(
                stepExecution.getStepName(),
                ImportBatchParameters.PARAM_DOIPOOLID,
                null);

        this.domObjDOIPool = this.doiPoolService.getDOIPool(doiPoolId);

        this.dataCiteClient.setAlternateUri("datacite-doi-compare-services-v2");
    }

    @Override
    public void write(final List<? extends DomObjDOI> domObjDOIList) throws Exception {
        try {
            for (DomObjDOI domObjDOI : domObjDOIList) {
                DublinCoreRecord dublinCoreRecord = domObjDOI.getMetadataRecord();
                DataCiteDataWrapper dataCiteDataWrapper = new DataCiteDataWrapper();
                DataCiteRecord dataCiteRecord = this.dataCiteMapper.map(dublinCoreRecord, this.domObjDOIPool);
                dataCiteDataWrapper.getData().setAttributes(dataCiteRecord);
                dataCiteDataWrapper.getData().setType("dois");
                dataCiteDataWrapper.getData().setId(domObjDOI.getDoi());

                try {
                    DataCiteDataWrapper dataCiteDataWrapper2 = this.dataCiteClient.getDOI(domObjDOI.getDoi());

                    if (!this.isEqual(dataCiteDataWrapper, dataCiteDataWrapper2)) {
                        this.batchExceptionHelper.storeComparisonError(
                                this.stepExecution.getJobExecutionId(),
                                this.domObjDOIPool,
                                domObjDOI,
                                objectMapper.writeValueAsString(dataCiteDataWrapper2.getData().getAttributes()));
                    }
                } catch (ServiceException e) {
                    LOG.warn("Could not compare DOI at DataCite: ", e);

                    if (DOIBatchErrors.BATCH_DATACITE_REQUEST_FAILED.equals(e.getErrorCode())) {
                        this.batchExceptionHelper.storeExportError(
                                e,
                                this.stepExecution.getJobExecutionId(),
                                this.domObjDOIPool,
                                domObjDOI,
                                objectMapper.writeValueAsString(dataCiteDataWrapper),
                                e.getErrors().get(0).getArguments()[1].toString());
                    } else {
                        this.batchExceptionHelper.storeExportError(
                                e,
                                this.stepExecution.getJobExecutionId(),
                                this.domObjDOIPool,
                                domObjDOI,
                                objectMapper.writeValueAsString(dataCiteDataWrapper),
                                null);
                    }
                } catch (Exception e) {
                    LOG.warn("Could not compare DOI at DataCite: ", e);

                    this.batchExceptionHelper.storeExportError(
                            e,
                            this.stepExecution.getJobExecutionId(),
                            this.domObjDOIPool,
                            domObjDOI,
                            objectMapper.writeValueAsString(dataCiteDataWrapper),
                            null);
                }

                if (this.stopSignal) {
                    break;
                }
            }
        } catch (Exception e) {
            LOG.warn("Could not compare DataCiteRecord: ", e);
        } finally {
            LOG.info("Comparedd #" + domObjDOIList.size() + " DataCite records...");
        }
    }

    @AfterStep
    public ExitStatus afterStep(final StepExecution stepExecution) throws Exception {
        if (this.stopSignal) {
            stepExecution.setStatus(org.springframework.batch.core.BatchStatus.STOPPED);
            return ExitStatus.STOPPED;
        } else if (stepExecution.getFailureExceptions().isEmpty()) {
            stepExecution.setStatus(org.springframework.batch.core.BatchStatus.COMPLETED);
            return ExitStatus.COMPLETED;
        }

        this.batchExceptionHelper.storeBatchFailures(stepExecution);

        stepExecution.setStatus(org.springframework.batch.core.BatchStatus.FAILED);
        return ExitStatus.FAILED;
    }

    @PreDestroy
    protected void preDestroy() {
        this.stopSignal = true;
    }

    private boolean isEqual(DataCiteDataWrapper dataCiteDataWrapper1, DataCiteDataWrapper dataCiteDataWrapper2)
            throws Exception {
        DataCiteRecord localRecord = dataCiteDataWrapper1.getData().getAttributes();
        DataCiteRecord remoteRecord = dataCiteDataWrapper2.getData().getAttributes();

        String localJson = objectMapper.writeValueAsString(localRecord);
        String remoteJson = objectMapper.writeValueAsString(remoteRecord);

        if (localJson.equals(remoteJson)) {
            return true;
        }

        return false;
    }
}
