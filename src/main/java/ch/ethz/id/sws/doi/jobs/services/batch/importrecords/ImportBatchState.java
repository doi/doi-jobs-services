package ch.ethz.id.sws.doi.jobs.services.batch.importrecords;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ImportBatchState {

    private String resumptionToken = null;

    private Integer offset = null;

    private String untilTimestamp = null;

    public static ImportBatchState createFromJSON(final String jsonObject) throws JsonProcessingException {
        final ObjectMapper objMapper = new ObjectMapper();
        return objMapper.readValue(jsonObject, ImportBatchState.class);
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setOffset(final Integer offset) {
        this.offset = offset;
    }

    public String getResumptionToken() {
        return this.resumptionToken;
    }

    public void setResumptionToken(final String resumptionToken) {
        this.resumptionToken = resumptionToken;
    }

    public String toJSON() throws JsonProcessingException {
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

    public String getUntilTimestamp() {
        return this.untilTimestamp;
    }

    public void setUntilTimestamp(String untilTimestamp) {
        this.untilTimestamp = untilTimestamp;
    }

    public void setUntilTimestamp(LocalDateTime localDateTime) {
        this.untilTimestamp = DateTimeFormatter.ISO_DATE_TIME.format(localDateTime);
    }

    @JsonIgnore
    public LocalDateTime getUntilTimestampAsLocalDateTime() {
        if (this.untilTimestamp != null) {
            return LocalDateTime.parse(this.untilTimestamp);
        }

        return null;
    }
}
