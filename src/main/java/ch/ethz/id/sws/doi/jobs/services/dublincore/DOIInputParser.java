package ch.ethz.id.sws.doi.jobs.services.dublincore;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;

import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;

public interface DOIInputParser {

    public List<DublinCoreRecord> parseRecords(final InputStream inputStream) throws Exception;

    public String getResumptionToken();

    public LocalDateTime getResponseDate();
}
