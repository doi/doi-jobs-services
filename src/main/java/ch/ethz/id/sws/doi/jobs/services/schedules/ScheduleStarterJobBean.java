package ch.ethz.id.sws.doi.jobs.services.schedules;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.commons.version.VersionManager;
import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParametersFactory;
import ch.ethz.id.sws.base.jobs.batch.services.BatchService;
import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob;
import ch.ethz.id.sws.base.jobs.schedule.services.DomObjJob.JobType;
import ch.ethz.id.sws.base.jobs.schedule.services.JobException;
import ch.ethz.id.sws.base.jobs.schedule.services.JobResult;
import ch.ethz.id.sws.base.jobs.schedule.services.JobService;
import ch.ethz.id.sws.base.jobs.services.jobexecution.DomObjJobExecution;
import ch.ethz.id.sws.base.jobs.services.jobinstance.DomObjJobInstance;
import ch.ethz.id.sws.base.jobs.services.jobinstance.DomObjJobInstanceResultat;
import ch.ethz.id.sws.base.jobs.services.jobinstance.DomObjJobInstanceSuche;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolResultat;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolSuche;
import ch.ethz.id.sws.doi.jobs.services.Constants;
import ch.ethz.id.sws.doi.jobs.services.batch.DOIBatchErrors;
import ch.ethz.id.sws.doi.jobs.services.batch.clear.ClearBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.compare.CompareBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.delete.DeleteBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.exportrecords.ExportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.fullexport.FullExportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.fullimport.FullImportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.fullsync.FullSyncBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.importrecords.ImportBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.relocaterecords.RelocateBatchParameters;
import ch.ethz.id.sws.doi.jobs.services.batch.update.UpdateBatchParameters;
import io.micrometer.core.instrument.util.StringUtils;

/** The ScheduleStarterJobBean is executed by Quartz in regular high frequency intervals as defined in the properties
 * file. It checks the table doipool for updates and creates, updates or deletes the cron schedule of a DOI pools
 * HarvesterJobBean. Such changes usually has their origins in the doi-online-services component. */
@Component
@DisallowConcurrentExecution
public class ScheduleStarterJobBean extends QuartzJobBean {

    private static Log LOG = LogFactory.getLog(ScheduleStarterJobBean.class);

    public final static String JOBPARAM_DOIPOOL = "DOIPool";

    private final static String JOBPARAM_RESTART_DONE = "RestartDone";

    private final static String JOBPARAM_ENCRYPT_DONE = "EncryptDone";

    private boolean restartBatches = false;

    @Autowired
    private final JobService jobService = null;

    @Autowired
    private final BatchService batchService = null;

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private final JobLauncher jobLauncher = null;

    @Autowired
    private final JobOperator jobOperator = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @Autowired
    private final VersionManager versionManager = null;

    @Autowired
    private final B3ApplicationContext b3ApplicationContext = null;

    @Autowired
    private final HarvesterScheduleListener harvesterExecutionListener = null;

    @PostConstruct
    protected void init() throws ServiceException {

        try {
            this.restartBatches = Boolean
                    .parseBoolean(this.b3ApplicationContext.getProperty(Constants.PROP_BATCH_RESTART, "false"));
        } catch (Exception e) {
            DOIBatchErrors.throwException(LOG, this.getClass().getSimpleName(),
                    new Object[] { Constants.PROP_BATCH_RESTART },
                    DOIBatchErrors.BATCH_UNDEFINED_RESTARTBATCHES);
        }

        this.harvesterExecutionListener.init();
    }

    @Override
    protected void executeInternal(final JobExecutionContext context) throws JobExecutionException {

        boolean isIdleRun = true;

        try {
            // Call only once after startup
            if (context.get(JOBPARAM_ENCRYPT_DONE) == null) {
                LOG.debug("Encrypting plaintext passwords in doipool table if any....");
                isIdleRun = isIdleRun && this.encryptNewPlainTextPasswords();
                context.put(JOBPARAM_ENCRYPT_DONE, Boolean.TRUE);
            }

            // Call only once after startup
            if (context.get(JOBPARAM_RESTART_DONE) == null && this.restartBatches) {
                LOG.debug("Restarting stopped batch jobs if any....");
                isIdleRun = isIdleRun && this.restartStoppedBatches();
                context.put(JOBPARAM_RESTART_DONE, Boolean.TRUE);
            }

            isIdleRun = isIdleRun && this.checkDOIPoolManualBatchRequests(context);
            isIdleRun = isIdleRun && this.checkDOIPoolCronUpdates(context);

            context.setResult(new JobResult(isIdleRun));
        } catch (final Exception e) {
            LOG.error("An exception has occurred in ScheduleStarterJobBean: ", e);
            throw new JobExecutionException(e);
        }
    }

    protected boolean checkDOIPoolManualBatchRequests(final JobExecutionContext context) {
        boolean isIdleRun = true;
        final DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        domObjDOIPoolSuche.setManualBatchOrder("");
        final DomObjDOIPoolResultat domObjDOIPoolResultat = this.doiPoolService.searchDOIPool(domObjDOIPoolSuche);

        for (final DomObjDOIPool domObjDOIPool : domObjDOIPoolResultat.getDoiPoolList()) {
            if (!StringUtils.isEmpty(domObjDOIPool.getBatchOrderOwner())
                    && !StringUtils.isEmpty(domObjDOIPool.getManualBatchOrder())) {
                isIdleRun = isIdleRun && this.createBatch(domObjDOIPool);
            }
        }

        return isIdleRun;
    }

    protected boolean checkDOIPoolCronUpdates(final JobExecutionContext context) throws ServiceException {
        boolean isIdleRun = true;

        final DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        domObjDOIPoolSuche.setHarvestCronDisabled(0);
        final DomObjDOIPoolResultat domObjDOIPoolResultat = this.doiPoolService.searchDOIPool(domObjDOIPoolSuche);

        final List<DomObjJob> domObjJobList = this.jobService.getAllJobs();

        for (final DomObjDOIPool domObjDOIPoolNew : domObjDOIPoolResultat.getDoiPoolList()) {
            if (StringUtils.isNotEmpty(domObjDOIPoolNew.getHarvestCronSchedule())) {
                // Find related Job if any
                DomObjJob domObjJobExisting = ScheduleStarterJobBean.findJobForPool(domObjDOIPoolNew.getId(),
                        domObjJobList);

                if (domObjJobExisting != null) {
                    // There is an existing cron job for the current DOI Pool
                    final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJobExisting.getStaticParams()
                            .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                    if (!isEqual(domObjDOIPool.getHarvestCronSchedule(), domObjDOIPoolNew.getHarvestCronSchedule())) {
                        // The DOI pool cron schedule has been changed meanwhile
                        // Stop current job and create new one
                        try {
                            isIdleRun = false;
                            this.jobService.remove(domObjJobExisting);
                            this.createHarvesterJob(domObjDOIPoolNew);

                            LOG.info("Harvest job updated for doiPoolId = " + domObjDOIPoolNew.getId()
                                    + ", name = '" + domObjDOIPoolNew.getName() + "'.");
                        } catch (final JobException e) {
                            LOG.error("Could not update job '" + domObjJobExisting.getDescription() + "': ", e);
                        }
                    }

                    domObjJobList.remove(domObjJobExisting);
                } else {
                    // There is no existing cron job for the current DOI Pool
                    // Create new job
                    isIdleRun = false;

                    this.createHarvesterJob(domObjDOIPoolNew);
                    LOG.info("New harvest job created for doiPoolId = " + domObjDOIPoolNew.getId() + ", name = '"
                            + domObjDOIPoolNew.getName() + "'.");
                }
            }
        }

        if (!domObjJobList.isEmpty()) {
            // Remove all remaining jobs that did not had equivalent entries in the
            // DOI pool list.
            for (final DomObjJob domObjJob : domObjJobList) {
                if (domObjJob.getJobType() == JobType.TRANSIENT) {
                    isIdleRun = false;

                    final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJob.getStaticParams()
                            .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                    try {
                        // Clear next run
                        this.jobService.remove(domObjJob);

                        LOG.info("Harvest job removed for doiPoolId = " + domObjDOIPool.getId() + ", name = '"
                                + domObjDOIPool.getName() + "'.");
                    } catch (final JobException e) {
                        LOG.error("Could not remove harvest job '" + domObjJob.getDescription() + "': ", e);
                    }
                }
            }
        }

        return isIdleRun;
    }

    protected boolean restartStoppedBatches() {
        boolean isIdleRun = true;

        try {
            DomObjJobInstanceSuche domObjJobInstanceSuche = new DomObjJobInstanceSuche();
            domObjJobInstanceSuche.getStatusList().add(BatchStatus.STOPPED.name());
            DomObjJobInstanceResultat domObjJobInstanceResultat = this.batchService.searchBatch(domObjJobInstanceSuche);

            for (DomObjJobInstance domObjJobInstance : domObjJobInstanceResultat.getJobInstanceList()) {
                List<DomObjJobExecution> executionList = domObjJobInstance.getJobExecutionList();

                if (!executionList.isEmpty()) {
                    DomObjJobExecution lastExecution = executionList.get(executionList.size() - 1);

                    try {
                        LocalDateTime appStartDateTime = Instant.ofEpochMilli(
                                this.versionManager.getAppStartupTime())
                                .atZone(ZoneId.systemDefault())
                                .toLocalDateTime();

                        if (lastExecution.getStartTime().isBefore(appStartDateTime)) {
                            this.jobOperator.restart(lastExecution.getId());
                            LOG.info("Restarted batch with executionId = " + lastExecution.getId() + ", name = "
                                    + domObjJobInstance.getJobName());
                        }
                    } catch (Exception e) {
                        LOG.warn("Restarted batch with executionId = " + lastExecution.getId(), e);
                    }
                }
            }
        } catch (Exception e) {
            LOG.warn("Could not restart batches: ", e);
        }

        return isIdleRun;
    }

    private DomObjJob createHarvesterJob(final DomObjDOIPool domObjDOIPool) {
        if (domObjDOIPool.getHarvestCronDisabled() == 0) {
            try {
                final DomObjJob domObjJob = new DomObjJob();
                domObjJob.setName("harvester-" + domObjDOIPool.getId());
                domObjJob.setDescription("Harvester job for pool '" + domObjDOIPool.getName() + "' ("
                        + domObjDOIPool.getId() + ")");
                domObjJob.setCronSchedule(domObjDOIPool.getHarvestCronSchedule());
                domObjJob.setClassname(ch.ethz.id.sws.doi.jobs.services.schedules.HarvesterJobBean.class.getName());
                domObjJob.setFirstDelay(0L);
                domObjJob.setGroup("Harvester");
                domObjJob.getStaticParams().put(JOBPARAM_DOIPOOL, domObjDOIPool);
                domObjJob.validate();
                domObjJob.setStartPaused(false);

                this.jobService.start(domObjJob);

                return this.jobService.getJobByName(domObjJob.getName());
            } catch (final JobException e) {
                LOG.error("Could not initialize harvest job - invalid harvest cron schedule at doiPoolId = "
                        + domObjDOIPool.getId() + ", cron = "
                        + domObjDOIPool.getHarvestCronSchedule(), e);
            }
        }

        return null;
    }

    private boolean createBatch(DomObjDOIPool domObjDOIPool) {
        boolean isIdleRun = true;

        if (this.hasRunningOrPendingBatch(domObjDOIPool.getId())) {
            return isIdleRun;
        }

        try {
            final BatchParametersFactory batchParamFactory = ((BatchJobRegistry) this.jobRegistry)
                    .getBatchParameters(domObjDOIPool.getManualBatchOrder());

            final Job job = this.jobRegistry.getJob(domObjDOIPool.getManualBatchOrder());
            final BatchParameters batchParams = batchParamFactory.createEmpty();

            switch (domObjDOIPool.getManualBatchOrder()) {
                case DOIPoolService.IMPORT_BATCH:
                    batchParams.setStepParam(
                            ImportBatchParameters.IMPORTSTEP_NAME,
                            ImportBatchParameters.PARAM_FROMTIMESTAMP,
                            domObjDOIPool.getLastImportDate());
                    batchParams.setStepParam(
                            null,
                            ImportBatchParameters.PARAM_DESCRIPTION,
                            "Manual import batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            ImportBatchParameters.IMPORTSTEP_NAME,
                            ImportBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            ImportBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            ImportBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.FULLIMPORT_BATCH:
                    batchParams.setStepParam(
                            null,
                            FullImportBatchParameters.PARAM_DESCRIPTION,
                            "Manual full import batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            ImportBatchParameters.IMPORTSTEP_NAME,
                            FullImportBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            FullImportBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            FullImportBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.FULLSYNC_BATCH:
                    batchParams.setStepParam(
                            null,
                            FullSyncBatchParameters.PARAM_DESCRIPTION,
                            "Manual fullsync batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            FullSyncBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            FullSyncBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            FullSyncBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.UPDATE_BATCH:
                    batchParams.setStepParam(
                            ImportBatchParameters.IMPORTSTEP_NAME,
                            UpdateBatchParameters.PARAM_FROMTIMESTAMP,
                            domObjDOIPool.getLastImportDate());
                    batchParams.setStepParam(
                            ExportBatchParameters.EXPORTSTEP_NAME,
                            UpdateBatchParameters.PARAM_FROMTIMESTAMP,
                            domObjDOIPool.getLastExportDate());
                    batchParams.setStepParam(
                            null,
                            UpdateBatchParameters.PARAM_DESCRIPTION,
                            "Manual update batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            UpdateBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            UpdateBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            UpdateBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.EXPORT_BATCH:
                    batchParams.setStepParam(
                            ExportBatchParameters.EXPORTSTEP_NAME,
                            ExportBatchParameters.PARAM_FROMTIMESTAMP,
                            domObjDOIPool.getLastExportDate());
                    batchParams.setStepParam(
                            null,
                            ExportBatchParameters.PARAM_DESCRIPTION,
                            "Manual export batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            ExportBatchParameters.EXPORTSTEP_NAME,
                            ExportBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            ExportBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            ExportBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.FULLEXPORT_BATCH:
                    batchParams.setStepParam(
                            null,
                            FullExportBatchParameters.PARAM_DESCRIPTION,
                            "Manual full export batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            ExportBatchParameters.EXPORTSTEP_NAME,
                            FullExportBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            FullExportBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            FullExportBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.RELOCATE_BATCH:
                    batchParams.setStepParam(
                            RelocateBatchParameters.RELOCATESTEP_NAME,
                            RelocateBatchParameters.PARAM_FROMTIMESTAMP,
                            null);
                    batchParams.setStepParam(
                            RelocateBatchParameters.RELOCATESTEP_NAME,
                            RelocateBatchParameters.PARAM_FROMTIMESTAMP,
                            domObjDOIPool.getLastImportDate());
                    batchParams.setStepParam(
                            null,
                            RelocateBatchParameters.PARAM_DESCRIPTION,
                            "Manual relocate batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            RelocateBatchParameters.RELOCATESTEP_NAME,
                            RelocateBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            RelocateBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            RelocateBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.COMPARE_BATCH:
                    batchParams.setStepParam(
                            null,
                            CompareBatchParameters.PARAM_DESCRIPTION,
                            "Manual compare batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            CompareBatchParameters.COMPARESTEP_NAME,
                            CompareBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            CompareBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            CompareBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.CLEAR_BATCH:
                    batchParams.setStepParam(
                            null,
                            ClearBatchParameters.PARAM_DESCRIPTION,
                            "Manual clear batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            ClearBatchParameters.CLEARSTEP_NAME,
                            ClearBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            ClearBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            ClearBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                case DOIPoolService.DELETE_BATCH:
                    batchParams.setStepParam(
                            null,
                            DeleteBatchParameters.PARAM_DESCRIPTION,
                            "Manual delete batch for doiPoolId = " + domObjDOIPool.getId());
                    batchParams.setStepParam(
                            DeleteBatchParameters.DELETESTEP_NAME,
                            DeleteBatchParameters.PARAM_DOIPOOLID,
                            domObjDOIPool.getId());
                    batchParams.setStepParam(
                            null,
                            DeleteBatchParameters.PARAM_CREATOR,
                            domObjDOIPool.getBatchOrderOwner());
                    batchParams.setStepParam(
                            null,
                            DeleteBatchParameters.PARAM_CREATIONTIMESTAMP,
                            LocalDateTime.now());
                    break;
                default:
                    break;
            }
            domObjDOIPool.setBatchOrderOwner(null);
            domObjDOIPool.setManualBatchOrder(null);
            this.doiPoolService.updateDOIPool(domObjDOIPool);

            final JobExecution jobExecution = this.jobLauncher.run(job, batchParams.toJobParameters());
            isIdleRun = false;

            LOG.info("ScheduleJobBean has started new manual batch job for doiPoolId = " + domObjDOIPool.getId()
                    + ", name = '" + domObjDOIPool.getName() + "' with executionId = " + jobExecution.getId());
        } catch (Exception e) {
            LOG.warn("An exception has occurred in HarvesterJobBean: ", e);
        }

        return isIdleRun;
    }

    protected boolean encryptNewPlainTextPasswords() {
        boolean isIdleRun = true;
        final DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        final DomObjDOIPoolResultat domObjDOIPoolResultat = this.doiPoolService.searchDOIPool(domObjDOIPoolSuche);

        for (final DomObjDOIPool domObjDOIPool : domObjDOIPoolResultat.getDoiPoolList()) {
            try {
                // Check if password is already encrypted
                if (domObjDOIPool.getDataCitePassword() != null && domObjDOIPool.getDataCitePassword().length > 0
                        && Arrays.equals(domObjDOIPool.getDataCitePassword(),
                                domObjDOIPool.getDataCitePasswordAsPlainText())) {
                    domObjDOIPool.encryptDataCitePassword();
                    this.doiPoolService.updateDOIPool(domObjDOIPool);

                    isIdleRun = false;
                    LOG.info("Encrypted plaintext password of doiPoolId = " + domObjDOIPool.getId());
                }
            } catch (Exception e) {
                LOG.warn("An exception has been occurred during password encryption of doiPoolId = "
                        + domObjDOIPool.getId() + ": ", e);
            }
        }

        return isIdleRun;
    }

    private static boolean isEqual(final String cron1, final String cron2) {
        if (cron1 == null && cron2 == null) {
            return true;
        }

        if (cron1 == null || cron2 == null) {
            return false;
        }

        return cron1.equals(cron2);
    }

    private boolean hasRunningOrPendingBatch(long doiPoolId) {
        List<JobExecution> batchList = this.batchService.getBatchRuntimeStatus();

        for (JobExecution jobExecution : batchList) {
            try {
                BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                        jobExecution);

                Long otherDoiPoolId = batchParams.getStepParamAsLong(
                        null,
                        ClearBatchParameters.PARAM_DOIPOOLID,
                        null);
                if (otherDoiPoolId == null) {
                    otherDoiPoolId = batchParams.getStepParamAsLong(
                            ImportBatchParameters.IMPORTSTEP_NAME,
                            ImportBatchParameters.PARAM_DOIPOOLID,
                            null);
                }
                if (otherDoiPoolId == null) {
                    otherDoiPoolId = batchParams.getStepParamAsLong(
                            ExportBatchParameters.EXPORTSTEP_NAME,
                            ExportBatchParameters.PARAM_DOIPOOLID,
                            null);
                }
                if (otherDoiPoolId == null) {
                    otherDoiPoolId = batchParams.getStepParamAsLong(
                            DeleteBatchParameters.DELETESTEP_NAME,
                            DeleteBatchParameters.PARAM_DOIPOOLID,
                            null);
                }

                if (otherDoiPoolId == doiPoolId) {
                    if (jobExecution.getStatus() != BatchStatus.ABANDONED &&
                            jobExecution.getStatus() != BatchStatus.COMPLETED &&
                            jobExecution.getStatus() != BatchStatus.FAILED &
                                    jobExecution.getStatus() != BatchStatus.UNKNOWN) {
                        return true;
                    }
                }
            } catch (Exception e) {
                LOG.info("Could not get batch params for executionId = " + jobExecution.getId(), e);
            }
        }

        return false;
    }

    private static DomObjJob findJobForPool(long doiPoolId, List<DomObjJob> domObjJobList) {
        // Find the related job schedule if any
        for (final DomObjJob domObjJob : domObjJobList) {
            if (domObjJob.getJobType() == JobType.TRANSIENT) {
                final DomObjDOIPool domObjDOIPool = (DomObjDOIPool) domObjJob.getStaticParams()
                        .get(ScheduleStarterJobBean.JOBPARAM_DOIPOOL);

                if (domObjDOIPool != null && domObjDOIPool.getId().equals(doiPoolId)) {
                    return domObjJob;
                }
            }
        }

        return null;
    }
}
