package ch.ethz.id.sws.doi.jobs.services.batch;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.validation.ObjectError;

public interface DOIBatchErrors {
    public static final String BATCH_IMPORTREQUEST_FAILED = "batch.import.1000"; // 0: url, 1: http status
    public static final String BATCH_RESPONSEDATE_INVALID = "batch.import.1010"; // 0: datestring
    public static final String BATCH_XMLPARSING_FAILED = "batch.import.1020"; // 0: sourcexml
    public static final String BATCH_XSLTTRANSFORMATION_FAILED = "batch.import.1030"; // 0: xslt, 1: sourcexml
    public static final String BATCH_INVALID_IMPORTTYPE = "batch.import.1040"; // 0: doi, 1: type

    public static final String BATCH_DATACITE_REQUEST_FAILED = "batch.export.1000"; // 0: doi, 1: response, 2: http
    public static final String BATCH_DATACITE_INVALID_CREDENTIALS = "batch.export.1010"; // 0: doi, 1: response, 2: http
    public static final String BATCH_DATACITE_FATAL_SERVER_ERROR = "batch.export.1020"; // 0: doi, 1: response, 2: http
    public static final String BATCH_DATACITE_TEMP_SERVER_ERROR = "batch.export.1030"; // 0: doi, 1: response, 2: http

    public static final String BATCH_UNDEFINED_UNIQUEID = "batch.config.1000"; // 0: prop name
    public static final String BATCH_UNDEFINED_RESTARTBATCHES = "batch.config.1010"; // 0: prop name
    public static final String BATCH_UNDEFINED_REPORTSENDER = "batch.config.1020"; // 0: prop name
    public static final String BATCH_UNDEFINED_SERVERURL = "batch.config.1030"; // 0: doi pool name
    public static final String BATCH_UNDEFINED_POOLSIZE = "batch.config.1040"; // 0: prop name
    public static final String BATCH_UNDEFINED_TIMEOUT = "batch.config.1050"; // 0: prop name
    public static final String BATCH_UNDEFINED_ADMINRECEIVER = "batch.config.1060"; // 0: prop namne

    public static final String BATCH_TECHNICAL_ERROR = "batch.global.1000"; // 0: msgs: 1, stacktrace

    public static final String BATCH_COMPARE_MISMATCH = "batch.compare.1000";

    public static void throwException(final Log LOG, String objName, final Object[] objects, final String errorCode)
            throws DOIBatchException {
        throwException(LOG, objName, objects, errorCode, null);
    }

    public static void throwException(final Log LOG, String objName, final Object[] objects, final String errorCode,
            Throwable t) throws DOIBatchException {
        if (objName == null) {
            objName = "<no-object>";
        }

        final ObjectError objError = new ObjectError(objName, new String[] {}, objects, errorCode);
        final List<ObjectError> errors = new ArrayList<>();
        errors.add(objError);

        throw new DOIBatchException(LOG, errorCode, errors, t);
    }
}
