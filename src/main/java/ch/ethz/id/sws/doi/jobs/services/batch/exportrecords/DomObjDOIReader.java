package ch.ethz.id.sws.doi.jobs.services.batch.exportrecords;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.jobs.batch.services.BatchJobRegistry;
import ch.ethz.id.sws.base.jobs.batch.services.BatchParameters;
import ch.ethz.id.sws.doi.commons.services.doi.DOIDAO;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.jobs.services.batch.BatchConfig;

public class DomObjDOIReader implements ItemReader<DomObjDOI>, ItemStream {

    private static Log LOG = LogFactory.getLog(DomObjDOIReader.class);

    @Autowired
    private final JobRegistry jobRegistry = null;

    @Autowired
    private DOIPoolService doiPoolService = null;

    @Autowired
    private DOIDAO doiDAO = null;

    private Long doiPoolId = null;

    private StepExecution stepExecution = null;
    private Boolean stopSignal = null;
    private Boolean deltaOnly = null;
    private List<DomObjDOI> doiList = null;
    private int listCounter = 0;
    private Long minDoiId = -1L;
    private LocalDateTime newLastExportDate = null;

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution) throws Exception {
        this.stopSignal = false;
        this.stepExecution = stepExecution;

        final BatchParameters batchParams = BatchParameters.fromJobParameters((BatchJobRegistry) this.jobRegistry,
                stepExecution.getJobExecution());

        this.doiPoolId = batchParams.getStepParamAsLong(
                stepExecution.getStepName(),
                ExportBatchParameters.PARAM_DOIPOOLID,
                null);

        this.deltaOnly = batchParams.getStepParamAsLocalDateTime(
                stepExecution.getStepName(),
                ExportBatchParameters.PARAM_FROMTIMESTAMP,
                null) != null;

        this.newLastExportDate = LocalDateTime.now();
    }

    @Override
    public void open(final ExecutionContext executionContext) throws ItemStreamException {
        LOG.info("Querying doi's to be exported in doiPoolId = " + this.doiPoolId);

        this.doiList = this.doiDAO.searchDOIToBeExported(
                this.doiPoolId,
                this.deltaOnly,
                this.minDoiId,
                2,
                0L,
                1000L);
    }

    @Override
    public void update(final ExecutionContext executionContext) throws ItemStreamException {
        // Nothing to do here
    }

    @Override
    public DomObjDOI read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (!this.stopSignal) {
            if (this.listCounter >= this.doiList.size()) {
                this.doiList = this.doiDAO.searchDOIToBeExported(
                        this.doiPoolId,
                        this.deltaOnly,
                        this.minDoiId,
                        2,
                        0L,
                        1000L);
                this.listCounter = 0;
            }

            if (this.listCounter < this.doiList.size()) {
                DomObjDOI domObjDOI = this.doiList.get(this.listCounter++);
                this.minDoiId = domObjDOI.getId();
                return domObjDOI;
            }
        }

        return null;
    }

    @Override
    public void close() throws ItemStreamException {
    }

    @AfterStep
    public void afterStep(final StepExecution stepExecution) {
        if (stepExecution.getFailureExceptions().isEmpty()) {
            DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(this.doiPoolId);

            try {
                domObjDOIPool.setLastExportDate(this.newLastExportDate);
                this.doiPoolService.updateDOIPool(domObjDOIPool);
            } catch (final Exception e) {
                LOG.fatal("Exception during doiPool update: ", e);
                throw new ItemStreamException(
                        "Could not execute beforeStep for executionId = " + stepExecution.getJobExecutionId() + ": ",
                        e);
            }
        }

        // The signal flag is confirmed here to ensure that all step-related tasks in
        // reader, processor and writer can be completed.
        this.stopSignal = null;
    }

    @PreDestroy
    protected void preDestroy() {
        if (this.stopSignal == null) {
            // Reader not yet started
            return;
        } else if (!this.stopSignal) {
            // Reader started
            LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                    + " received stop signal - stopping job...");

            this.stopSignal = true;
        }

        long waitStart = System.currentTimeMillis();
        while (System.currentTimeMillis() - waitStart < BatchConfig.MAX_SHUTDOWN_WAITTIME) {
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId()
                        + " non-gracefully stopped.");
                break;
            }

            if (this.stopSignal == null) {
                LOG.info("Batch with executionId = " + this.stepExecution.getJobExecutionId() + " gracefully stopped.");
                break;
            }
        }
    }
}
